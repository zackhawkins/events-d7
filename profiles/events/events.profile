<?php
function events_form_install_configure_form_alter(&$form, $form_state) {
  drupal_get_messages('warning');

}
function events_install_tasks(){
  $tasks['events_install_date_formats'] = array(
    'display_name' => st('Creating date formats'),
  );

  return $tasks;
}
function events_install_date_formats(){
variable_set('date_format_box_date', 'D n/j');
variable_set('date_format_list_date', 'l n/j');
variable_set('date_format_mdy', 'm/d/y');
variable_set('date_format_proper', 'l, F j g:i a');
variable_set('date_format_unix', 'U');

}
function events_date_format_types(){
	return array(
	'box_date' => 'box date',
	'list_date'=>'list date',
	'mdy'=>'MDY',
	'proper'=>'Proper',
	'unix'=>'Unix',
	);
}
function events_date_formats(){
	return array(
	array(
		'type'=>'mdy',
		'format'=>'m/d/y',
	),
	array(
		'type'=>'box_date',
		'format'=>'D n/j',
	),
	array(
		'type'=>'list_date',
		'format'=>'l n/j',
	),
	array(
		'type'=>'proper',
		'format'=>'l, F j g:i a',
	),
	array(
		'type'=>'unix',
		'format'=>'U',
	),
	);
}






