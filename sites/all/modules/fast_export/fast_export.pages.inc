<?php
function fast_export_kills($type){
	$query = db_select('fast_export_kill_record', 'fk');
	$query->fields('fk',array('timestamp','type_string','id'));
    $query->condition('type_string', $type);
    $result = $query->execute();
	$fast_id=variable_get('fast_editorial_business_unit_identifier', '');
    while($record = $result->fetchAssoc()) {
		print $fast_id.'-CAL-'.$record['id']."\n";		
    }
	
}