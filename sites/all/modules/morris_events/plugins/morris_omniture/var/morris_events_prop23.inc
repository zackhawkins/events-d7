<?php
/**
 * @file
 * JavaScript morris_events_prop23 plugin.
 */

$plugin = array(
  'class' => 'morris_events_omniture_prop23',
  'title' => 'prop23',
  'group' => 'Morris Events',
  'overridable' => FALSE,
);

class morris_events_omniture_prop23 extends morris_omniture_var_base {

  /**
   * Get plugin value.
   * 
   * @return StdClass
   *   JavaScript code snippet wrapper.
   */
  public function val() {
      
    $str = "(function() {

            var lparr = location.pathname.split('/');
            var toppath = lparr[1];

            return (toppath == '')?'Events Calendar':'Events Content';

        })();";

    return morris_omniture_js_snippet_wrapper($str);

  }

}