<?php
/**
 * Hooks provided by the yahoo apt module.
 */

/*
 * Modify the Yahoo APT build.
 * 
 * This is useful for adding geographical or
 * user based targeting information which could really
 * reside anywhere considering the data would not be part
 * of the core.
 * 
 * Appropriate use cases here would be using user location
 * to population geo targeting data to send to APT or 
 * additional user fields for things like income, age, etc.
 * 
 * @implement hook_yahoo_apt_shared_params_alter
 */
function hook_yahoo_apt_shared_params_alter(&$params) {
	$params->user_state = 'MI';
}

/**
 * Override morris communication APT section options.
 *
 * Custom sections should not be changed once mappings have been created
 * unless orphaned data is cleaned-up from the yahoo_apt_mapping_custom_section table.
 *
 * @implement hook_yahoo_apt_custom_sections_alter
 */
function yahoo_apt_custom_sections_alter(&$custom_sections) {
	$custom_sections = array();
	
	// My account custom sections
	// ...
}

/*
 * Override morris communications APT content topics
 *
 * Content topics should not be changed once mappings have been created
 * unless orphaned data is cleaned-up from the yahoo_apt_mapping_content_topic table.
 *
 * @implement hook_yahoo_apt_content_topics_alter
 */
function yahoo_apt_content_topics_alter(&$content_topics) {
	$content_topics = array();
	
	// My account content topics
	// ...
}