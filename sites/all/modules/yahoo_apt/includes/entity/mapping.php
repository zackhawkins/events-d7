<?php

/**
 * Domain Mapping entity
 */
class YahooApt_Mapping {
	
	protected
	$id,
	$context,
	$requestType = 'ac',
	$clickDestination = '_blank',
	$disableContentSend = 0,
	$dma,
	$excludeRichMedia = 0,
	$contentTopics = array(),
	$contentTypes = array(),
	$customSections = array(),
	$adspots = array();
	
	public function setId($id) {
		$this->id = $id;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function setContext($context) {
		$this->context = $context;
	}
	
	public function getContext() {
		return $this->context;
	}
	
	public function setRequestType($requestType) {
		$this->requestType = $requestType;
	}
	
	public function getRequestType() {
		return $this->requestType;
	}
	
	public function setClickDestination($clickDestination) {
		$this->clickDestination = $clickDestination;
	}
	
	public function getClickDestination() {
		return $this->clickDestination;
	}
	
	public function setDisableContentSend($disableContentSend) {
		$this->disableContentSend = $disableContentSend;
	}
	
	public function getDisableContentSend() {
		return $this->disableContentSend;
	}
	
	public function setDma($dma) {
		$this->dma = $dma;
	}
	
	public function getDma() {
		return $this->dma;
	}
	
	public function setExcludeRichMedia($excludeRichMedia) {
		$this->excludeRichMedia = $excludeRichMedia;
	}
	
	public function getExcludeRichMedia() {
		return $this->excludeRichMedia;
	}
	
	public function addContentTopic($contentTopic) {
		$this->contentTopics[] = $contentTopic;
	}
	
	public function getContentTopics() {
		return $this->contentTopics;
	}
	
	public function hasContentTopic($id) {
		return $this->_hasRelationalItem('contentTopics',$id);
	}
	
	public function addContentType($contentType) {
		$this->contentTypes[] = $contentType;
	}
	
	public function getContentTypes() {
		return $this->contentTypes;
	}
	
	public function hasContentType($id) {
		return $this->_hasRelationalItem('contentTypes',$id);
	}
	
	public function addCustomSection($customSection) {
		$this->customSections[] = $customSection;
	}
	
	public function getCustomSections() {
		return $this->customSections;
	}
	
	public function hasCustomSection($id) {
		return $this->_hasRelationalItem('customSections',$id);
	}
	
	public function addAdspot($adspot) {
		$this->adspots[] = $adspot;
	}
	
	public function getAdspots() {
		return $this->adspots;
	}
	
	public function hasAdspot($id) {
		return $this->_hasRelationalItem('adspots',$id);
	}
	
	protected function _hasRelationalItem($property,$value,$id='id') {
		foreach($this->{$property} as $item) {
			if($item->{$id} == $value) {
				return true;
			}
		}
		return false;
	}
	
}