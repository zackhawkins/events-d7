<?php

class YahooApt_ad {
	
	protected
	
	$id,
	$name,
	$deliveryMode = 'ipatf',
	$marker = 't',
	$redirectClickWrapper,
	$customContentCategories = array(),
	$reportingTags = array(),
	$sizes = array();
	
	public function __construct() {
	}
	
	public function setId($id) {
		$this->id = $id;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function setName($name) {
		$this->name = $name;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function setDeliveryMode($deliveryMode) {
		$this->deliveryMode = $deliveryMode;
	}
	
	public function getDeliveryMode() {
		return $this->deliveryMode;
	}
	
	public function setMarker($marker) {
		$this->marker = $marker;
	}
	
	public function getMarker() {
		return $this->marker;
	}
	
	public function setRedirectClickWrapper($redirectClickWrapper) {
		$this->redirectClickWrapper = $redirectClickWrapper;
	}
	
	public function getRedirectClickWrapper() {
		return $this->redirectClickWrapper;
	}
	
	public function addCustomContentCategory($customContentCategory) {
		$this->customContentCategories[] = $customContentCategory;
	}
	
	public function getCustomContentCategories() {
		return $this->customContentCategories;
	}
	
	public function hasCustomContentCategory($id) {
		return $this->_hasRelationalItem('customContentCategories',$id);
	}
	
	public function addReportingTag($reportingTag) {
		$this->reportingTags[] = $reportingTag;
	}
	
	public function getReportingTags() {
		return $this->reportingTags;
	}
	
	public function hasReportingTag($id) {
		return $this->_hasRelationalItem('reportingTags',$id);
	}
	
	public function addSize($size) {
		$this->sizes[] = $size;
	}
	
	public function getSizes() {
		return $this->sizes;
	}
	
	public function hasSize($id) {
		return $this->_hasRelationalItem('sizes',$id);
	}
	
	protected function _hasRelationalItem($property,$value,$id='id') {
		foreach($this->{$property} as $item) {
			if($item->{$id} == $value) {
				return true;
			}
		}
		return false;
	}
	
}