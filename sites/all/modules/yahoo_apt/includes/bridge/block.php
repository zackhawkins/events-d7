<?php

/**
 * @implement hook_block_info
 */
function yahoo_apt_block_info() {
	
	/**
	 * Get all ads
	 */
	$result = db_select('yahoo_apt_ad','a')
	->fields('a',array('id','name'))
	->execute();
	
	$blocks = array();
	
	/**
	 * Convert ads to blocks. Take special note
	 * that the name is used for the block delta. This
	 * is necessary so that the block associated with
	 * an adspot can be identified. Though this requires
	 * that the ad name does not change once it has been created.
	 */
	while($ad=$result->fetchObject()) {
		$blocks[$ad->name] = array(
			'info'=> "Yahoo APT Ad: {$ad->name}",
			'cache'=> DRUPAL_NO_CACHE
		);
	}
	
	return $blocks;
	
}

/**
 * @implement hook_block_view
 */
function yahoo_apt_block_view($delta) {
	
	/**
	 * Register ad
	 */
	yahoo_apt_request_register_ad($delta);
	
	/**
	 * Block content
	 *
	 * One might debate whether it is best to use tokens for this but I think tokens is a overkill. Not
	 * to mention these replacement strings should not be available for just anyone to add. The only
	 * purpose they serve is to get around issues with panels where blocks are themed before all data
	 * is available. All data will be available during the page #post_render stage so that is where
	 * the block will actually be themed and placeholder specified here will be replaced.
	 */
	return array(
		'subject'=> null, //"Yahoo APT Ad: $delta", - might be needed for panels - need to test
		'content'=> "<!-- yahoo-apt-ad-$delta-block -->" /* replacement occurs using page #post_render otherwise ads would not function with panels */
	);
}