<?php

/**
 * @implement hook_permission
 */
function yahoo_apt_permission() {

	$perms = array();
	
	/**
	 * Manage ad entities
	 */
	$perms['administer yahoo apt ads'] = array(
		'title'=> t('Administer Yahoo Ads'),
		'description'=> t('Administer yahoo graphical and rich media Ads.'),
		'warning'=> t('You do not have permission to administer Yahoo ads.')
	);
	
	/**
	 * Manage mapping entities
	 */
	$perms['administer yahoo apt mappings'] = array(
		'title'=> t('Administer Yahoo Ad Mappings'),
		'description'=> t('Administer page mappings to Yahoo graphical and rich media ads.'),
		'warning'=> t('You do not have permission to administer Yahoo ad mappings.')
	);
	
	return $perms;

}