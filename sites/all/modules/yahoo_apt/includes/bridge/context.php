<?php

/**
 * @implement hook_context_plugins (context)
 */
function yahoo_apt_context_plugins() {
	$plugins = array();
	$plugins['yahoo_apt_context_reaction_mapping'] = array(
		'handler' => array(
			'path' => drupal_get_path('module', 'yahoo_apt') .'/plugins/context',
			'file' => 'yahoo_apt_context_reaction_mapping.inc',
			'class' => 'yahoo_apt_context_reaction_mapping',
			'parent' => 'context_reaction',
		)
 	);
	return $plugins;
}

/**
 * @implement hook_context_registry (context)
 */
function yahoo_apt_context_registry() {	
	return array(
		'reactions' => array(
			'yahoo_apt_mapping' => array(
				'title' => t('Ad Mapping'),
				'description'=> 'Ad mapping',
				'plugin' => 'yahoo_apt_context_reaction_mapping'
			)
		)
	);
}

/**
 * @implement hook_context_load
 */
function yahoo_apt_context_load_alter($context) {
	
	/**
	 * This does not seem to be a good solution considering all
	 * contexts are being loaded on every page...
	 */
	//drupal_set_message($context->name);
	
}