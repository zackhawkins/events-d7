<?php

/**
 * @implement hook_help
 * 
 * This is a simple implementation to load standard help snippets
 * from the help/core directory. This approach seems cleaner than
 * stuffing HTML in the module file itself.
 */
function yahoo_apt_help($path,$arg) {
	
	//drupal_set_message('<pre>'.print_r(domain_get_domain(),true).'</pre>');
	
	/**
	 * Location of help docs
	 */
	$help_root = str_replace('\\','/',DRUPAL_ROOT).'/'.drupal_get_path('module','yahoo_apt').'/help/core/';
	
	/**
	 * Help document mappings
	 */
	$docs = array( // not really necessary with advent of help docs
		/*array(
			'path'=> 'admin/config/content/yahoo-apt',
			'comparision'=> 'strcmp',
			'file'=> 'mapping_defaults.html'
		)*/
		/*array(
			'path'=> 'admin/structure/yahoo-apt/mappings/paths',
			'comparision'=> 'strcmp',
			'file'=> 'ad_datagrid.html'
		),
		array(
			'path'=> 'admin/structure/yahoo-apt/mappings/terms',
			'comparision'=> 'strcmp',
			'file'=> 'ad_datagrid.html'
		)*/
	);
	
	/*
	 * File to load for page request if any.
	 */
	$file = null;
	
	/**
	 * Map request to help docs.
	 */
	foreach($docs as $doc) {
		
		// strict string comparision against path
		if(strcmp('strcmp',$doc['comparision']) === 0 && strcmp($path,$doc['path']) === 0) {
			$file = $doc['file'];
			break;
			
		// Add support for other comparisions as necessary
		} else if(true) {
			
		} else {
			
		}
		
	}
	
	/**
	 * Get document contents
	 */
	if($file && file_exists($help_root.$file)) {
		
		/**
		 * This method is used so that both PHP and static HTML file
		 * can be used.
		 */
		ob_start();
		include($help_root.$file);
		$content = ob_get_contents();
		ob_end_clean();
		
		return $content;
		
	}
	
}