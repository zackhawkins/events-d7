<?php

/**
 * @implement hook_menu
 */
function yahoo_apt_menu() {

	$items = array();
	
	/**
	 * Piggy back on the system module to display second tier 
	 * of administrative advertising tasks.
	 */
	$items['admin/structure/yahoo-apt'] = array(
		'title'=> 'Advertising',
		'description'=> 'Manage Yahoo APT advertising.',
		'page callback'=> 'system_admin_menu_block_page',
		'access arguments'=> array('administer yahoo apt ads'),
		'file'=> 'system.admin.inc',
		'file path'=> drupal_get_path('module','system')
	);
	
	/**
	 * Second tier administrative tasks.
	 * - Ads
	 * - Mappings
	 */
	 $items['admin/structure/yahoo-apt/ads'] = array(
		'title'=> 'Adspots',
		'description'=> 'Administer Yahoo graphical and rich media ad spots.',
		'page callback'=> 'yahoo_apt_ad_datagrid_page',
		'access arguments'=> array('administer yahoo apt ads'),
	 	'file path'=> drupal_get_path('module','yahoo_apt').'/includes/admin',
		'file'=> 'ad_datagrid.php'
	 );
	 
	 $items['admin/structure/yahoo-apt/mappings'] = array(
		'title'=> 'Mappings',
		'description'=> 'Administer page mappings for overriding ad spot configuration.',
		'page callback'=> 'system_admin_menu_block_page',
		'access arguments'=> array('administer yahoo apt mappings'),
	 	'file'=> 'system.admin.inc',
		'file path'=> drupal_get_path('module','system')
	 );
	 
	 // path mappings
	 $items['admin/structure/yahoo-apt/mappings/paths'] = array(
		'title'=> 'Path Mappings',
		'description'=> 'Administer path mappings for overriding ad spot configuration.',
		'page callback'=> 'yahoo_apt_mapping_datagrid_page',
	 	'page arguments'=> array(4), // type of mappings
		'access arguments'=> array('administer yahoo apt mappings'),
	 	'file path'=> drupal_get_path('module','yahoo_apt').'/includes/admin',
		'file'=> 'mapping_datagrid.php'
	 );
	 
	 // term mappings
	 $items['admin/structure/yahoo-apt/mappings/terms'] = array(
		'title'=> 'Term Mappings',
		'description'=> 'Administer term mappings for overriding ad spot configuration.',
		'page callback'=> 'yahoo_apt_mapping_datagrid_page',
	 	'page arguments'=> array(4), // type of mappings
		'access arguments'=> array('administer yahoo apt mappings'),
	 	'file path'=> drupal_get_path('module','yahoo_apt').'/includes/admin',
		'file'=> 'mapping_datagrid.php'
	 );
	 
	 /**
	  * Individual high level entity form add entry points.
	  */
	$items['admin/structure/yahoo-apt/ads/add'] = array(
		'title'=> 'Create New Adspot',
		'description'=> 'Create new ad spot.',
		'page callback'=> 'drupal_get_form',
		'page arguments'=> array('yahoo_apt_ad_form'),
		'access arguments'=> array('administer yahoo apt ads'),
		'file'=> 'includes/form/ad.php',
		'type'=> MENU_LOCAL_ACTION
	);
	
	/**
	 * The whole goal here is to make the transition from CASAA to this module
	 * easier. That requires essentially customizing the context form to better
	 * represent the forms that existed in casaa for the apt plugin module. This
	 * is achieved by handing off the request to the standard ctools export ui
	 * page handler as if the standard context edit path was being used. The only difference
	 * is an extra argument which is used in the form alter hook to determine whether
	 * the form is being used within our context. A little tricky but I think well worth
	 * the user experience improvement and cutting off people from standard context form
	 * which can be very confussing. This essentially hides all the stuff that is not relevant
	 * to the daily operations of ad mappings for terms and paths, like casaa did but handled
	 * all by context module. The same thing goes for the edit path below.
	 * 
	 * Also mappings do not need to be created through here. They can be created through the standard
	 * context form. This is just a cleaned up version only presenting options applicable to ad mapppings
	 * while leaving everything else hidden.
	 */
	$items['admin/structure/yahoo-apt/mappings/paths/add'] = array(
		'title'=> 'Add Path Mapping',
		'description'=> 'Create new ad spot path mapping.',
		'page callback'=> 'ctools_export_ui_switcher_page',
		'page arguments'=> array('yahoo_apt_path_mapping','add',null,null,'yahoo_apt_path_mapping'), // context of mapping
		'load arguments'=> array('context'),
		'access arguments'=> array('administer yahoo apt mappings'),
		'file path'=> drupal_get_path('module','ctools'),
		'file'=> 'includes/export-ui.inc',
		'type'=> MENU_LOCAL_ACTION
	);
	
	$items['admin/structure/yahoo-apt/mappings/terms/add'] = array(
		'title'=> 'Add Term Mapping',
		'description'=> 'Create new ad spot term mapping.',
		'page callback'=> 'ctools_export_ui_switcher_page',
		'page arguments'=> array('yahoo_apt_term_mapping','add',null,null,'yahoo_apt_term_mapping'), // context of mapping
		'load arguments'=> array('context'),
		'access arguments'=> array('administer yahoo apt mappings'),
		'file path'=> drupal_get_path('module','ctools'),
		'file'=> 'includes/export-ui.inc',
		'type'=> MENU_LOCAL_ACTION
	);
	
	/**
	 * Ad and mapping edit forms.
	 */
	$items['admin/structure/yahoo-apt/ads/%yahoo_apt_ad/edit'] = array(
		'title'=> 'Edit Adspot',
		'description'=> 'Create new ad spot.',
		'page callback'=> 'drupal_get_form',
		'page arguments'=> array('yahoo_apt_ad_form',4),
		'access arguments'=> array('administer yahoo apt ads'),
		'file'=> 'includes/form/ad.php'
	);
	
	/**
	 * Use the standard ctools export ui form.
	 */
	$items['admin/structure/yahoo-apt/mappings/terms/%ctools_export_ui/edit'] = array(
		'title'=> 'Edit Term Mapping',
		'description'=> 'Create new ad spot page mapping.',
		'page callback'=> 'ctools_export_ui_switcher_page',
		'page arguments'=> array('yahoo_apt_term_mapping','edit',5,null,'yahoo_apt_term_mapping'), // context of mapping
		'load arguments'=> array('context'),
		'access arguments'=> array('administer yahoo apt mappings')
	);
	
	$items['admin/structure/yahoo-apt/mappings/paths/%ctools_export_ui/edit'] = array(
		'title'=> 'Edit Path Mapping',
		'description'=> 'Create new ad spot page mapping.',
		'page callback'=> 'ctools_export_ui_switcher_page',
		'page arguments'=> array('yahoo_apt_path_mapping','edit',5,null,'yahoo_apt_path_mapping'), // context of mapping
		'load arguments'=> array('context'),
		'access arguments'=> array('administer yahoo apt mappings')
	);
	
	/**
	 * Global config settings
	 */
	$items['admin/config/content/yahoo-apt'] = array(
		'title'=> 'Yahoo APT Ad Serving',
		'description'=> 'Default yahoo APT settings applied to pages that do match any other mappings. When domain access and domain context modules are installed each site will have separate default settings. Merely visit this same page using the other various domains and change the settings as applicable to individual site.',
		'page callback'=> 'yahoo_apt_settings_page',
		'access arguments'=> array('administer yahoo apt mappings'),
		'file path'=> drupal_get_path('module','yahoo_apt').'/includes/admin',
		'file'=> 'settings.php'
	);
	
	return $items;

}