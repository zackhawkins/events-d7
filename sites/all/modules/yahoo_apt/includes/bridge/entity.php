<?php

/**
 * @implement hook_entity_info
 * 
 */
function yahoo_apt_entity_info() {
	
	$info = array();
	
	// mapping
	$info['yahoo_apt_mapping'] = array(
		'label'=> 'Ad Mapping',
		'controller class'=> 'YahooApt_Mapping_Repository',
		'base table'=> 'yahoo_apt_mapping',
		'load hook'=> 'yahoo_apt_mapping_load',
		'uri callback'=> 'yahoo_apt_mapping_uri',
		'fieldable'=> false,
		'entity keys'=> array(
			'id'=> 'id',
			'label'=> 'id'
		)
	);
	
	// ad
	$info['yahoo_apt_ad'] = array(
		'label'=> 'Ad',
		'controller class'=> 'YahooApt_Ad_Repository',
		'base table'=> 'yahoo_apt_ad',
		'load hook'=> 'yahoo_apt_ad_load',
		'uri callback'=> 'yahoo_apt_ad_uri',
		'fieldable'=> false,
		'entity keys'=> array(
			'id'=> 'id',
			'label'=> 'name'
		)
	);
	
	return $info;
	
}