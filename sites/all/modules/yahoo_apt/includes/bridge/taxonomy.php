<?php

/**
 * @implement hook_taxonomy_term_delete
 * 
 * When a taxonomy term belonging to either the reporting tag
 * or custom content categories vocabularies is deleted remove any term references
 * from the associated apt tables ie. yahoo_apt_ad_custom_content_category, yahpoo_apt_reporting_tag 
 * 
 * In theory this is not required because Drupal schema lacks support for actual
 * foreign keys. None the less, it is always a good idea to clean up orphaned data references.
 * 
 * There is not much that can be done about deleting terms directly with a query. If that happens
 * orphans will exist.
 * 
 * It really sucks that Drupal does not have a way to delete multiple terms at once calling this
 * hook for every term that will be deleted but I guess that is just the way it is. Luckily it
 * will not be called to often.
 */
function yahoo_apt_taxonomy_term_delete($term) {
	
	$name = $term->vocabulary_machine_name;
	
	if(strcasecmp($name,YAHOO_APT_TAX_VOCAB_CUSTOM_CONTENT_CATEGORIES_MACHINE_NAME) === 0) {
		
		// remove any content category references
		$num = db_delete('yahoo_apt_ad_custom_content_category')->condition('term_id',$term->tid)->execute();
		
		// Set message for number of deleted references
		drupal_set_message("$num Yahoo APT Ad Custom Content Category reference(s) have been deleted. This is a clean-up process that removes the custom content category from being sent to APT for adspots that it was assigned.");
		
	} else if(strcasecmp($name,YAHOO_APT_TAX_VOCAB_REPORTING_TAGS_MACHINE_NAME) === 0) {
		
		// remove any reporting tag references
		$num = db_delete('yahoo_apt_ad_reporting_tag')->condition('term_id',$term->tid)->execute();
		
		// Set message to number of deleted references
		drupal_set_message("$num Yahoo APT Ad Reporting Tag reference(s) have been deleted. This is a clean-up process that removes the reporting tag from being sent to APT for adspots that it was assigned.");
		
	}
	
}

/**
 * @implement hook_form_term_confirm_delete_alter
 * 
 * I think it is a smart idea to prompt users deleting terms belonging to
 * the reporting tag or custom content category vocabulary that any ad spots
 * which had those terms associated with them will have the terms removed and
 * not sent o apt. This would probably seem obvious to some but probably not those
 * that are not very technical. So it is worthy of at least a message I believe.
 */
function yahoo_apt_form_taxonomy_form_term_alter(&$form,&$form_state) {
	
	/**
	 * When term form is being generated in context of term delete mode
	 * add our message.
	 */
	if(!isset($form_state['confirm_delete']) || !$form_state['confirm_delete']) {
		return;
	}
	
	/**
	 * Only deal with the proper vocabs
	 */
	$name = $form_state['term']->vocabulary_machine_name;
	if(strcasecmp($name,YAHOO_APT_TAX_VOCAB_CUSTOM_CONTENT_CATEGORIES_MACHINE_NAME) === 0) {
		
		$form['yahoo_apt_ref_delete'] = array(
			'#markup'=> '<p><strong>IMPORTANT:</strong> When custom content category is deleted it will also be removed from any yahoo apt adspots that it was assigned. This message is generated on behalf of the Yahoo APT module clean-up process to prevent orphaned data.</p>'
		);
		
	} else if(strcasecmp($name,YAHOO_APT_TAX_VOCAB_REPORTING_TAGS_MACHINE_NAME) === 0) {
		
		$form['yahoo_apt_ref_delete'] = array(
			'#markup'=> '<p><strong>IMPORTANT:</strong> When reporting tag is deleted it will also be removed from any yahoo apt adspots that it was assigned. This message is generated on behalf of the Yahoo APT module clean-up process to prevent orphaned data.</p>'
		);
		
	}
	
}
