<?php

/**
 * Mapping repository
 * 
 * @todo:
 * 
 * load:
 * Cache by context_id: when ids are null and one condition for context_id
 */
class YahooApt_Mapping_Repository implements DrupalEntityControllerInterface {
	
	public function __construct($entityType) {
		
	}
	
	public function resetCache(array $ids = null) {
		
	}
	
	public function load($ids = array(), $conditions = array()) {
		
		// drupal_set_message('mapping load');
		
		$query = $this->_makeBaseSelectQuery($conditions);
		
		$query->addTag('yahoo_apt_mapping_load');
		
		$this->_includeContentTopics($query,$conditions);
		$this->_includeContentTypes($query,$conditions);
		$this->_includeCustomSections($query,$conditions);
		$this->_includeAdspots($query,$conditions);
		
		if(!empty($ids)) {
			$query->condition('a.id',$ids);
		}
		
		/**
		 * Tag query
		 */
		//$query->addTag('yahoo_apt_mapping_load');
		
		$result = $query->execute();
		
		$mappings = $this->_mapSelectResult($result);
		
		return $mappings;
		
	}
	
	/**
	 * Save mapping to persistent storage mechanism.
	 * 
	 * @param YahooApt_Mapping
	 */
	public function persist(YahooApt_Mapping $mapping) {
		
		//drupal_set_message('persist mapping');
		//drupal_set_message('<pre>'.print_r($mapping,true).'</pre>');
		
		$transaction = db_transaction();
		
		try {
			
			$data = array(
				'context_id'=> $mapping->getContext()->name,
				'request_type'=> $mapping->getRequestType(),
				'click_destination'=> $mapping->getClickDestination(),
				'disable_content_send'=> $mapping->getDisableContentSend(),
				'exclude_rich_media'=> $mapping->getExcludeRichMedia()
			);
		
			if($mapping->getId() !== null) {
				
				db_update('yahoo_apt_mapping')
				->fields($data)
				->condition('id',$mapping->getId())
				->execute();
				
			} else {
				
				$id = db_insert('yahoo_apt_mapping')
				->fields($data)
				->execute();
				
				$mapping->setId($id);
				
			}
				
			$this->_persistContentTopics($mapping);
			$this->_persistContentTypes($mapping);
			$this->_persistCustomSections($mapping);
			$this->_persistAdspots($mapping);
			
		} catch(Exception $e) {
			$transaction->rollback();
			throw $e;			
		}
		
	}
	
	/**
	 * Physically remove mappings and dependencies from database.
	 */
	public function purgeById($ids) {
		
		if(empty($ids)) {
			return;
		}
		
		/**
		 * Gather all the names of context that need to be deleted
		 */
		$context = array();
		$query = db_select('yahoo_apt_mapping','m');
		$query->condition('m.id',$ids);
		$query->fields('m',array('context_id'));
		$result = $query->execute();
		
		while($row=$result->fetchAssoc()) {
			$context[] = $row['context_id'];
		}
		
		//echo drupal_set_message('<pre>'.print_r($context,true).'</pre>');
		//return;
		
		$transaction = db_transaction();
		
		try {
			
			/**
			 * Delete from tables dependent on mapping.
			 */
			foreach(array('content_type','content_topic','custom_section','ad') as $name) {
				$query = db_delete("yahoo_apt_mapping_$name");
				$query->condition('mapping_id',$ids);
				$query->execute();
			}
		
			/**
			 * Delete the mapping itself
			 */
			$query = db_delete('yahoo_apt_mapping');
			$query->condition('id',$ids);
			$r = $query->execute();
			
			/**
			 * Delete context
			 * 
			 * Implementation pulled directly from context_delete though this
			 * uses the query builder rather than hard-coded query.
			 */
			$query = db_delete('context');
			$query->condition('name',$context);
			$query->execute();
			context_invalidate_cache();
			
			return $r;
			
		} catch(Exception $e) {
			$transaction->rollback();
			throw $e;
		}
		
	}
	
	/**
	 * Create base select query.
	 */
	protected function _makeBaseSelectQuery($conditions) {	
		
		$query = db_select('yahoo_apt_mapping','m');	
		$query->fields('m');
		
		if(isset($conditions['mapping'])) {
			foreach($conditions['mapping'] as $field=>$value) {
				if(strpos($field,'.') === false) {
					$query->condition("m.$field",$value);
				} else {
					$query->condition($field,$value);
				}
			}
		}
		
		return $query;
		
	}
	
	/**
	 * Include content topics in query.
	 */
	protected function _includeContentTopics($query,$conditions) {
		$query->leftJoin('yahoo_apt_mapping_content_topic','t','m.id = %alias.mapping_id');
		$query->addField('t','id','content_topic_id');
		$query->addField('t','value','content_topic_value');
	}
	
	/**
	 * Include content types in query.
	 */
	protected function _includeContentTypes($query,$conditions) {
		$query->leftJoin('yahoo_apt_mapping_content_type','t2','m.id = %alias.mapping_id');	
		$query->addField('t2','id','content_type_id');
		$query->addField('t2','value','content_type_value');
	}
	
	/**
	 * Include custom sections in query
	 */
	protected function _includeCustomSections($query,$conditions) {
		$query->leftJoin('yahoo_apt_mapping_custom_section','s','m.id = %alias.mapping_id');
		$query->addField('s','id','custom_section_id');
		$query->addField('s','value','custom_section_value');
	}
	
	/**
	 * Include enabled adpots in query
	 */
	protected function _includeAdspots($query,$conditions) {
		$query->leftJoin('yahoo_apt_mapping_ad','a','m.id = %alias.mapping_id');
		$query->addField('a','id','mapping_ad_id');
		$query->addField('a','ad_id','mapping_ad_ad_id');
		
		// join against ad table for name
		$query->leftJoin('yahoo_apt_ad','p','a.ad_id = %alias.id');
		$query->addField('p','name','mapping_ad_name');
		
	}
	
	/**
	 * Map raw result to entity object(s).
	 * 
	 * @param obj
	 */
	protected function _mapSelectResult($result) {
		
		/**
		 * Map raw data to create collection of mappings.
		 */
		$mappings = array();
		
		/**
		* Define how has many relationships will be mapped to domain object.
		*/
		$assoc = array(
			'content_topic'=> 'ContentTopic',
			'content_type'=> 'ContentType',
			'custom_section'=> 'CustomSection',
			'mapping_ad'=> 'Adspot'
		);
		
		/**
		 * Map raw data to domain object
		 */
		while($row=$result->fetchAssoc()) {
			
			if(!isset($mappings[$row['id']])) {
				
				// Create new domain object
				$mapping = new YahooApt_Mapping();
				
				// Map row to domain object
				$mapping->setId($row['id']);
				$mapping->setRequestType($row['request_type']);
				$mapping->setClickDestination($row['click_destination']);
				$mapping->setDisableContentSend($row['disable_content_send']);
				$mapping->setDma($row['dma']);
				$mapping->setExcludeRichMedia($row['exclude_rich_media']);
				
				/**
				 * Associated context
				 */
				$mapping->setContext((object) array(
					'name'=> $row['context_id']
				));
				
			} else {
				$mapping = $mappings[$row['id']];
			}
			
			/**
			 * Map associative properties.
			 */
			foreach($assoc as $name=>$base) {
				if(isset($row["{$name}_id"]) && !$mapping->{"has$base"}($row["{$name}_id"])) {
					if(strpos($name,'mapping_ad') !== false) {
						$mapping->{"add$base"}( (object) array(
							'id'=> $row["{$name}_id"],
							'ad_id'=> $row["{$name}_ad_id"],
							'name'=> $row["{$name}_name"]
						));
					} else {
						$mapping->{"add$base"}( (object) array(
							'id'=> $row["{$name}_id"],
							'value'=> $row["{$name}_value"]
						));
					}
				}
			}
			
			if(!isset($mappings[$row['id']])) {
				$mappings[$row['id']] = $mapping;
			}
			
		}
		
		return $mappings;
		
	}
	
	/**
	 * Save mapping content topics to persistent storage mechanism.
	 * 
	 * @param YahooApt_Mapping
	 */
	protected function _persistContentTopics(YahooApt_Mapping $mapping) {
		
		$insert = null;
		
		// Remove all existing content topics
		$delete = db_delete('yahoo_apt_mapping_content_topic');
		$delete->condition('mapping_id',$mapping->getId());
		$delete->execute();
		
		foreach($mapping->getContentTopics() as $item) {
			
			$data = array(
				'value'=> $item->value
			);
				
			if($insert === null) {
				$insert = db_insert('yahoo_apt_mapping_content_topic');
				$insert->fields(array('mapping_id','value'));
			}
				
			$data['mapping_id'] = $mapping->getId();
				
			$insert->values($data);
		}
		
		if($insert !== null) {
			$insert->execute();	
		}
		
	}
	
	/**
	 * Save mapping content types to persistent storage mechanism.
	 * 
	 * @param YahooApt_Mapping
	 */
	protected function _persistContentTypes(YahooApt_Mapping $mapping) {
		
		$insert = null;
		
		// Remove all existing content types
		$delete = db_delete('yahoo_apt_mapping_content_type');
		$delete->condition('mapping_id',$mapping->getId());
		$delete->execute();
		
		foreach($mapping->getContentTypes() as $item) {
			
			$data = array(
				'value'=> $item->value
			);
				
			if($insert === null) {
				$insert = db_insert('yahoo_apt_mapping_content_type');
				$insert->fields(array('mapping_id','value'));
			}
				
			$data['mapping_id'] = $mapping->getId();
				
			$insert->values($data);
		}
		
		if($insert !== null) {
			$insert->execute();	
		}
		
	}
	
	/**
	 * Save mapping custom sections to persistent storage mechanism.
	 * 
	 * @param YahooApt_Mapping
	 */
	protected function _persistCustomSections(YahooApt_Mapping $mapping) {
		
		$insert = null;
		
		// Remove all existing custom sections
		$delete = db_delete('yahoo_apt_mapping_custom_section');
		$delete->condition('mapping_id',$mapping->getId());
		$delete->execute();
		
		foreach($mapping->getCustomSections() as $item) {
			
			$data = array(
				'value'=> $item->value
			);
				
			if($insert === null) {
				$insert = db_insert('yahoo_apt_mapping_custom_section');
				$insert->fields(array('mapping_id','value'));
			}
				
			$data['mapping_id'] = $mapping->getId();
				
			$insert->values($data);
		}
		
		if($insert !== null) {
			$insert->execute();	
		}
		
	}
	
	/**
	 * Save enabled adspots to persistent storage mechanism.
	 * 
	 * @param YahooApt_Mapping
	 */
	protected function _persistAdspots(YahooApt_Mapping $mapping) {
		
		$insert = null;
		
		// Remove all existing enabled adspots
		$delete = db_delete('yahoo_apt_mapping_ad');
		$delete->condition('mapping_id',$mapping->getId());
		$delete->execute();
		
		foreach($mapping->getAdSpots() as $item) {
			
			$data = array(
				'ad_id'=> $item->ad_id
			);
				
			if($insert === null) {
				$insert = db_insert('yahoo_apt_mapping_ad');
				$insert->fields(array('mapping_id','ad_id'));
			}
				
			$data['mapping_id'] = $mapping->getId();
				
			$insert->values($data);
		}
		
		if($insert !== null) {
			$insert->execute();	
		}
		
	}
	
}