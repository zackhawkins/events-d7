<?php

/**
 * Ad form
 */
function yahoo_apt_ad_form($form,&$form_state,$ad=null) {

	if(!isset($ad)) {
		$ad = new YahooApt_Ad();
	}
	
	$form['#ad'] = $ad;
	
	$form['name'] = array(
		'#type'=> 'machine_name',
		'#machine_name' => array(
			'exists' => 'yahoo_apt_ad_load_by_name',
		),
		'#title'=> 'Name',
		'#required'=> true,
		'#default_value'=> $ad->getName(),
		'#description'=> 'Unique name used to indentify ad spot throughout site.',
		
		/**
		 * Name can not be changed after creation.
		 */
		'#disabled'=> $ad->getId() !== null
	);
	
	$form['ad'] = array(
		'#type'=> 'vertical_tabs',
		'#default_tab'=> 'general_tab'
	);
	
	$form['ad']['general_tab'] = array(
		'#type'=> 'fieldset',
		'#title'=> 'General Info'
	);
	
	$form['ad']['general_tab']['delivery_mode'] = array(
		'#type'=> 'select',
		'#title'=> 'Delivery Mode',
		'#required'=> true,
		'#default_value'=> $ad->getDeliveryMode(),
		'#options'=> array(
			'ipstf'=> 'Spans the fold',
			'ipatf'=> 'Above the fold',
			'ipbtf'=> 'Below the fold'
		),
		'#description'=> 'The ad delivery mode describes where or when the ad is being shown to the consumer.'
	);
	
	$form['ad']['general_tab']['marker'] = array(
		'#type'=>'radios',
		'#title'=> 'Advertisement Text',
		'#default_value'=> in_array($ad->getMarker(),array('t','b'))?$ad->getMarker():'n',
		'#options'=> array(
			'n'=> 'None',
			't'=> 'Top',
			'b'=> 'Bottom'
		),
		'#description'=> 'The ad marker is a line of ad text that appears above or below an ad, that runs the width of the ad. This a good way to visually differeniate ads from real content.'
	);
	
	$form['ad']['general_tab']['redirect_click_wrapper'] = array(
		'#type'=> 'textfield',
		'#title'=> 'Redirect Click Wrapper',
		'#element_validate'=> array('yahoo_apt_ad_validate_redirect_click_wrapper'),
		'#default_value'=> $ad->getRedirectClickWrapper(),
		'#description'=> 'Specify a valid URL here to send users to before proceeding to an individual advertisers page when ad is clicked. This is most useful for application specific click tracking.'
	);
	
	$form['ad']['custom_content_categories_tab'] = array(
		'#type'=> 'fieldset',
		'#title'=> 'Custom Content Categories'
	);
	
	$ccc = entity_get_controller('taxonomy_vocabulary')->load(null,array('machine_name'=>YAHOO_APT_TAX_VOCAB_CUSTOM_CONTENT_CATEGORIES_MACHINE_NAME));
	$vocab = array_pop($ccc);
	$form['ad']['custom_content_categories_tab']['custom_content_categories'] = array(
		'#type'=> 'hierarchical_select',
		'#title'=> 'Custom Content Categories',
	    '#config' => array(
		  'module'=> 'hs_taxonomy',
	      'params' => array(
				'vid'=> $vocab->vid
		  ),
	      'save_lineage'    => 0,
	      'enforce_deepest' => 0,
	      'entity_count'    => 0,
	      'require_entity'  => 0,
	      'resizable'       => 0,
	      'level_labels' => array(
	        'status' => 0,
	        'labels' => array(),
	      ),
	      'dropbox' => array(
	        'status'   => 1,
	        'title'    => t('All Custom Content Categories'),
	        'limit'    => 10,
	        'reset_hs' => 1,
	      ),
	      'editability' => array(
	        'status'           => 1,
	        'item_types'       => array(),
	        'allowed_levels'   => array(),
	        'allow_new_levels' => 1,
	        'max_levels'       => null,
	      ),
	      'allow_new_levels' => 0,
	      'render_flat_select'=> 0
	    ),
	    '#default_value' => array(),
	    '#description'=> ''
	);
	
	$form['ad']['reporting_tags_tab'] = array(
		'#type'=> 'fieldset',
		'#title'=> 'Reporting Tags'
	);
	
	$rpt = entity_get_controller('taxonomy_vocabulary')->load(null,array('machine_name'=>'yahoo_apt_reporting_tags'));
	$vocab = array_pop($rpt);
	$form['ad']['reporting_tags_tab']['reporting_tags'] = array(
		'#type'=> 'hierarchical_select',
		'#title'=> 'Reporting Tags',
	    '#config' => array(
		  'module'=> 'hs_taxonomy',
	      'params' => array(
				'vid'=> $vocab->vid
		  ),
	      'save_lineage'    => 0,
	      'enforce_deepest' => 0,
	      'entity_count'    => 0,
	      'require_entity'  => 0,
	      'resizable'       => 0,
	      'level_labels' => array(
	        'status' => 0,
	        'labels' => array(),
	      ),
	      'dropbox' => array(
	        'status'   => 1,
	        'title'    => t('All Reporting Tags'),
	        'limit'    => 10,
	        'reset_hs' => 1,
	      ),
	      'editability' => array(
	        'status'           => 1,
	        'item_types'       => array(),
	        'allowed_levels'   => array(),
	        'allow_new_levels' => 1,
	        'max_levels'       => null,
	      ),
	      'allow_new_levels' => 0,
	      'render_flat_select'=> 0
	    ),
	    '#default_value' => array(),
	    '#description'=> ''
	);
	
	/**
	 * Load custom content categories and reporting tags
	 */
	foreach($ad->getCustomContentCategories() as $item) {
		$form['ad']['custom_content_categories_tab']['custom_content_categories']['#default_value'][] = $item->term->id;
	}
	
	foreach($ad->getReportingTags() as $item) {
		$form['ad']['reporting_tags_tab']['reporting_tags']['#default_value'][] = $item->term->id;
	}
	
	/**
	 * How this is done is very complex. The best bet to udnerstanding what is being done
	 * here to allow adding ad sizes through the form is to visit this link:
	 * 
	 * http://drupal.org/node/48643
	 * 
	 * Than go inspect the field modules file: field_ui/field_ui.admin.inc | function: field_ui_field_overview_form
	 * 
	 * I pretty much copied the concept directly from there.
	 * 
	 * It took me a long time to figure this out and is not at all easy to understand until you understand
	 * how the field ui form is built with a nested table.
	 */
	$form['ad']['sizes_tab'] = array(
		'#type'=> 'fieldset',
		'#title'=> 'Sizes'
	);
	
	$form['ad']['sizes_tab']['ad_sizes_wrapper'] = array(
		'#type'=> 'container',
		'#attributes'=> array('id'=>'ad-sizes-wrapper')
	);
	
	$form['ad']['sizes_tab']['ad_sizes_wrapper']['ad_sizes'] = array(
		'#tree'=> true,
		'#attributes' => array('class' => array('draggable', 'tabledrag-leaf', 'add-new')),
		'#type'=> 'yahoo_apt_ad_form_ad_size_table',
		'#header'=> array(	
			'w/h'=> 'wxh',
			'width'=> 'Width',
			'height'=> 'Height',
			'priority'=> 'Priority',
			'delete'=> 'Delete'
		),
		'#rows'=> array(),
		'#empty'=> 'No Sizes Have been Defined',
		'#caption'=> 'Ad Position Sizes'
	);
	
	$form['actions'] = array('#type' => 'actions');
	$form['actions']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save Ad'),
		'#weight' => 40,
	);
	
	/**
	 * Load ad sizes as table rows w/ hidden inputs
	 * for rebuilding size list upon form submission.
	 */
	$sizes = array();
	
	if(isset($form_state['values']) &&  strcasecmp($form_state['values']['op'],'Add Size') === 0) {
		foreach($form_state['values']['ad_sizes'] as $size) {
			if(
				(isset($size['id']) && empty($size['delete'])) || // existing size
				(!isset($size['id']) && !empty($size['width']) && !empty($size['height'])) // new size
			) {
				$sizes[] = (object) array(
					'id'=> '',
					'width'=> $size['width'],
					'height'=> $size['height'],
					'priority'=> !empty($size['priority'])?$size['priority']:null,
					'weight'=> 0 // @todo
				);				
			}
		}
	} else {
		$sizes = $ad->getSizes();
	}
	
	foreach($sizes as $index=>$item) {
		$form['ad']['sizes_tab']['ad_sizes_wrapper']['ad_sizes']["{$index}"] = array(
			'wxh_wrapper'=>array(
				'#type'=> 'container',
				'label'=> array(
					'#markup'=> "{$item->width}x{$item->height}"
				),
				'id'=> array(
					'#type'=> 'hidden',
					'#value'=> $item->id,
					'#parents'=> array('ad_sizes',"{$index}",'id')
				)
			),
			'width_wrapper'=>array(
				'#type'=> 'container',
				'label'=> array(
					'#markup'=> $item->width
				),
				'width'=> array(
					'#type'=> 'hidden',
					'#value'=> $item->width,
					'#parents'=> array('ad_sizes',"{$index}",'width')
				)
			),
			'height_wrapper'=>array(
				'#type'=> 'container',
				'label'=> array(
					'#markup'=> $item->height
				),
				'height'=> array(
					'#type'=> 'hidden',
					'#value'=> $item->height,
					'#parents'=> array('ad_sizes',"{$index}",'height')
				)
			),
			'priority_wrapper'=>array(
				'#type'=> 'container',
				'label'=> array(
					'#markup'=> $item->priority?$item->priority:'N/A'
				),
				'priority'=> array(
					'#type'=> 'hidden',
					'#value'=> $item->priority,
					'#parents'=> array('ad_sizes',"{$index}",'priority')
				)
			),
			'delete'=>array(
				'#type'=> 'checkbox',
				'#return_value'=> 1
			)
		);
	}
	
	/**
	 * Add new size
	 */
	$form['ad']['sizes_tab']['ad_sizes_wrapper']['ad_sizes']['new'] = array(
		'wxh'=> array(
			'#markup'=> '&nbsp;'
		),
		'width'=>array(
			'#type'=> 'textfield',
			'#maxlength'=> 5,
			'#attributes'=> array('size'=>5),
			'#title'=> 'Width',
			'#title_display'=> 'invisible',
		),
		'height'=> array(
			'#type'=> 'textfield',
			'#maxlength'=> 5,
			'#attributes'=> array('size'=> 5),
			'#title'=> 'Height',
			'#title_display'=> 'invisible'
		),
		'priority'=>array(
			'#type'=> 'textfield',
			'#maxlength'=> 2,
			'#attributes'=> array('size'=> 2),
			'#title'=> 'Priority',
			'#title_display'=> 'invisible'
		),
		'add'=>array(
			'#type'=> 'button',
			'#value'=> 'Add Size',
			'#ajax'=> array(
				'callback'=> 'yahoo_apt_ajax_ad_size_table',
				'wrapper'=> 'ad-sizes-wrapper',
				'method'=> 'replace'
			)
		)
	);
	
	return $form;
	
}

/**
 * Ad form submit handler.
 */
function yahoo_apt_ad_form_submit(&$form,&$form_state) {
	
	//drupal_set_message('<pre>'.print_r($form_state['values'],true).'</pre>');
	//return;
	
	
	// Get ad object
	$ad = new YahooApt_Ad();
	$ad->setId($form['#ad']->getId());
	
	// Override ad
	$form['#ad'] = $ad;
	
	// Set data
	$ad->setName($form_state['values']['name']);
	$ad->setDeliveryMode($form_state['values']['delivery_mode']);
	$ad->setMarker(in_array($form_state['values']['marker'],array('t','b'))?$form_state['values']['marker']:null);
	$ad->setRedirectClickWrapper(empty($form_state['values']['redirect_click_wrapper'])?null:$form_state['values']['redirect_click_wrapper']);
	
	// Map custom content categories
	foreach($form_state['values']['custom_content_categories'] as $term_id) {
		$ad->addCustomContentCategory((object) array(
			'term'=> (object) array(
				'id'=> (int) $term_id
			)
		));
	}
	
	// Map reporting tags
	foreach($form_state['values']['reporting_tags'] as $term_id) {
		$ad->addReportingTag((object) array(
			'term'=> (object) array(
				'id'=> (int) $term_id
			)
		));
	}
	
	// Map sizes
	foreach($form_state['values']['ad_sizes'] as $size) {
		
		/**
		 * 1.) 
		 * Any prexisting ad sizes that have not been marked for deletion
		 * get readded. Any sizes that have been marked for delete will be ignored
		 * and thus deleted.
		 * 
		 * 2.) 
		 * When a new size has been specified add it. Any new sizes must at least
		 * have a width and height otherwise they will be ignored.
		 */
		if(
			(isset($size['id']) && empty($size['delete'])) || // existing size
			(!isset($size['id']) && !empty($size['width']) && !empty($size['height'])) // new size
		) {
			
			$ad->addSize((object) array(
				'width'=> $size['width'],
				'height'=> $size['height'],
				'priority'=> !empty($size['priority'])?$size['priority']:null,
				'weight'=> 0 // @todo
			));		
			
		}
		
	}
	
	//drupal_set_message('<pre>'.print_r($ad,true).'</pre>');
	//return;
	
	// Save to db
	entity_get_controller('yahoo_apt_ad')->persist($ad);
		
	/**
	 * When add new size button is clicked go back to the form without redirecting.
	 */
	//if(isset($form_state['values']['op']) && strcasecmp($form_state['values']['op'],'Add Size') === false) {
		// redirect
		$form_state['redirect'] = array('admin/structure/yahoo-apt/ads');
	//}
	
	drupal_set_message('Ad has been saved successfully!');
	
}

/**
 * AJAX callback for ad size table submit button.
 */
function yahoo_apt_ajax_ad_size_table(&$form,&$form_state) {
	
	/**
	 * Make name optional to prevent error message.
	 */
	$form['name']['#required'] = false;
	
	/**
	 * Reset value
	 */
	$form['ad']['sizes_tab']['ad_sizes_wrapper']['ad_sizes']['new']['width']['#value'] = '';
	$form['ad']['sizes_tab']['ad_sizes_wrapper']['ad_sizes']['new']['height']['#value'] = '';
	$form['ad']['sizes_tab']['ad_sizes_wrapper']['ad_sizes']['new']['priority']['#value'] = '';
	
	/**
	 * 
	 */
	drupal_set_message('Sizes will not be added or deleted until ad is saved.','warning');
	
	/**
	 * Rebuild ad sizes table
	 */
	return $form['ad']['sizes_tab']['ad_sizes_wrapper'];
	
}

/**
 * Custom validation routine for redirect click wrapper.
 *
 * Validation of the redirect click wrapper will be done by using CURL to make
 * sure the requested URL does not issue a 404.
 */
function yahoo_apt_ad_validate_redirect_click_wrapper($element,&$form_state,$form) {
	
	 if(!empty($element['#value'])) {
		//$ch = curl_init(); // @todo validate URL with cURL check for 404
	 }
	
}

/**
 * Theme ad form ad size table.
 */
function theme_yahoo_apt_ad_form_ad_size_table($variables) {
	
	$rows = array();
	
	foreach(element_children($variables['elements']) as $child) {
		$cells = array();
		foreach($variables['elements'][$child] as $item) {
			if(is_array($item) && (isset($item['#type']) || isset($item['#markup'])) ) {
				$cell = array('data'=>$item);
				$cells[] = $cell;
			}
		}
		$rows[] = $cells;
	}
	
	$variables['elements']['#rows'] = $rows;
	return theme('table',$variables['elements']);
	
}