<?php

/*
 * ----------------------------------------------------------------------------------------
 * Mappings admin data grid
 * ----------------------------------------------------------------------------------------
 */

/**
 * List of mappings
 * 
 * @param str type of mapping
 */
function yahoo_apt_mapping_datagrid_page($type) {
	
	$form_state = array();
	$form_state['build_info']['args'] = array($type);
	
	// Rebuild filter state from session data
	switch($type) {
		case 'terms':
			// Assign form state for filter from session
			$form_state['values']['filters'] = array(
				'term'=> isset($_SESSION['yahoo_apt'],$_SESSION['yahoo_apt']['datagrid'],$_SESSION['yahoo_apt']['datagrid'][$type])?$_SESSION['yahoo_apt']['datagrid'][$type]['filter']['term']:null
			);
			break;
		
		case 'paths':
		default:
			// Assign form state for filter from session
			$form_state['values']['filters'] = array(
				'path'=> isset($_SESSION['yahoo_apt'],$_SESSION['yahoo_apt']['datagrid'],$_SESSION['yahoo_apt']['datagrid'][$type])?$_SESSION['yahoo_apt']['datagrid'][$type]['filter']['path']:null
			);
	}
	
	// build out form
	return drupal_build_form('yahoo_apt_mapping_datagrid_form',$form_state);
}

/**
 * Mapping data grid form
 */
function yahoo_apt_mapping_datagrid_form($form,&$form_state,$type) {
	
	//drupal_set_message('<pre>'.print_r($_SESSION['yahoo_apt']['datagrid']['filter'],true).'</pre>');
	
	/**
	 * Load mapping data
	 * 
	 * @todo: Would like to move this to repo but idk...
	 */
	$query = db_select('yahoo_apt_mapping','m')->extend('PagerDefault')->extend('TableSort');
	$query->innerJoin('context','c','m.context_id = %alias.name');
	
	$query->fields('m',array('id','context_id'));
	
	/**
	 * Serialized data... yuck
	 */
	$query->addField('c','conditions','context_conditions');
	
	/**
	 * Filter based on mapping type and change out label for data grid.
	 */
	switch($type) {
		case 'terms':
			$query->condition('c.tag','yahoo_apt_term_mapping');
			$headers = array(	
				'tid'=> t('tid'),
				'vocab'=> t('Vocab'),
				'term'=> t('Term'),
				'operations'=> t('Operations')
			);
			
			/**
			 * Apply filters
			 * 
			 * This is really a terrible way to filter but given what we have to work
			 * with their really aren't to many options.
			 * 
			 * this really sucks... i hate serialized data.. idk what to do here...
			 * short of creating a column for tid within the yahoo_apt_mapping table. Screw
			 * it for now.
			 */
			if(!empty($form_state['values']['filters']['term'])) {
				// node taxonomy match
				//$query->condition('c.conditions','%s:13:"node_taxonomy";%','LIKE');
				
				// term ID match
				//$query->condition("c.conditions",'%'.$form_state['values']['filters']['term'].'%','LIKE');
			}
			
			break;
			
		case 'paths':
		default:
			$query->condition('c.tag','yahoo_apt_path_mapping');
			$headers = array(
				'path'=> t('Path'),
				'operations'=> t('Operations')
			);
			
			/**
			 * Apply filters
			 * 
			 * The best we can really do here due to the serialized nature of the data is
			 * search against the entire value using like. Not much of a better way to do
			 * this without storing the path separately in the mapping table which I think
			 * is best avoided. Nature of using other peopels stupid decisions like serializing
			 * data and stuffing it into a single column...
			 */
			if(!empty($form_state['values']['filters']['path'])) {
				$query->condition("c.conditions",'%'.$form_state['values']['filters']['path'].'%','LIKE');
			}
			
			//drupal_set_message((string) $query);
			
			break;
	}
	
	$result = $query->execute();
	
	//drupal_set_message((string) $query);
	
	/**
	 * Collect row data
	 */
	$rows = array();
	while($mapping=$result->fetchAssoc()) {
		
		/**
		 * Unserialize mapping conditions
		 */
		$conditions = unserialize($mapping['context_conditions']);
		
		// echo '<pre>'.print_r($mapping,true).'</pre>';
		
		$rows[$mapping['id']] = array(
			'info'=> array(
				'data'=> array(
					'#markup'=> ''
				)
			),
			'operations'=> array(
				'data'=> array(
					'#type'=> 'link',
					'#href'=> "admin/structure/yahoo-apt/mappings/{$mapping['context_id']}/edit",
					'#title'=> 'Edit'
				)
			)
		);
		
		switch($type) {
			
			case 'terms':
				
				// echo '<pre>'.print_r($mapping,true).'</pre>';
				
				// For the time being only recognize the first term
				$tid = array_shift($conditions['node_taxonomy']['values']);
				
				// Get term ancestory including self
				$terms = array_reverse(taxonomy_get_parents_all($tid),false);
				
				$info = '';
				
				// Build out label based on ancestory path
				foreach($terms as $index=>$term) {
					if(strlen($info) !== 0) $info.= ' > ';
					$info.= $term->name;
				}
				
				// term tid
				$rows[$mapping['id']]['tid']['data']['#markup'] = $terms[count($terms)-1]->tid;
				
				// term vocabulary
				$rows[$mapping['id']]['vocab']['data']['#markup'] = $terms[count($terms)-1]->vocabulary_machine_name;
				
				// term path
				$rows[$mapping['id']]['term']['data']['#markup'] = $info;
				break;
			
			case 'paths':
			default:
				// For the time being only display the first path though mutiples are possible
				$rows[$mapping['id']]['path']['data']['#markup'] = array_shift($conditions['path']['values']);
		}
		
		$rows[$mapping['id']]['operations'] = array(
			'data'=> array(
				'#type'=> 'link',
				'#href'=> "admin/structure/yahoo-apt/mappings/$type/{$mapping['context_id']}/edit",
				'#title'=> 'Edit'
			)
		);
		
	}
	
	/**
	 * Build out form
	 */
	$form = array();
	
	$form['mappings'] = array(
		'#type'=> 'tableselect',
		'#header'=> $headers,
		'#options'=> $rows,
		'#empty'=> t('No mappings available.')
	);
	
	/* pager */
	$form['pager'] = array('#markup' => theme('pager'));
	
	/**
	 * Operations
	 */
	$form['operations'] = array(
		'#type'=> 'fieldset',
		'#title'=> 'Operations',
		'#weight'=> -1,
		'#tree'=> true,
		'#collapsed'=> true,
		'#collapsible'=> true
	);
	
	$form['operations']['operation'] = array(
		'#type'=> 'select',
		'#options'=> array(
			'delete'=> t('Delete')
		),
		'#empty_option'=> '--',
		'#empty_value'=> 0
	); 
	
	$form['operations']['submit'] = array(
		'#type'=> 'submit',
		'#value'=> 'Update',
		'#submit'=> array('yahoo_apt_mapping_datagrid_form_submit_operation')
	);
	
	/** Filter form
	 * 
	 */
	$form['filters'] = array(
		'#type'=> 'fieldset',
		'#title'=> 'Search',
		'#weight'=> -2,
		'#tree'=> true,
		'#collapsed'=> true,
		'#collapsible'=> true
	);
	
	$form['filters']['submit'] = array(
		'#type'=> 'submit',
		'#value'=> 'Search',
		'#submit'=> array('yahoo_apt_mapping_datagrid_form_submit_filter'),
		'#weight'=> 100
	);
	
	$form['filters']['clear'] = array(
		'#type'=> 'submit',
		'#value'=> 'Clear',
		'#submit'=> array('yahoo_apt_mapping_datagrid_form_submit_filter'),
		'#weight'=> 101
	);
	
	switch($type) {
		case 'terms':
			/*$form['filters']['vocab'] = array(
				'#type'=> 'textfield',
				'#title'=> 'Vocabulary'
			);*/
			
			$form['filters']['term'] = array(
				'#type'=> 'textfield',
				'#title'=> 'Term ID',
				'#default_value'=> $form_state['values']['filters']['term']
			);
			
			// kill for now -- not functioning -- serialized data makes me want to cry
			$form['filters']['#access'] = false;
			
			break;
			
		case 'paths':
		default:
			$form['filters']['path'] = array(
				'#type'=> 'textfield',
				'#title'=> 'Path',
				'#default_value'=> $form_state['values']['filters']['path']
			);
	}
	
	return $form;
	
}

/**
 * Operation submit handler
 */
function yahoo_apt_mapping_datagrid_form_submit_operation($form,&$form_state) {
	
	$mappings = array();
	
	/**
	 * Create collection of mappings to operate on.
	 */
	foreach($form_state['values']['mappings'] as $id=>$v) {
		if($v != 0) {
			$mappings[] = $id;
		}
	}
	
	/**
	 * Kill if nothing to delete
	 */
	if(empty($mappings)) {
		return;
	}
	
	/**
	 * Run operation on mappings.
	 */
	switch($form_state['values']['operations']['operation']) {
		case 'delete':
			$deleted = entity_get_controller('yahoo_apt_mapping')->purgeById($mappings);
			drupal_set_message("Yahoo APT mappings have been sucessfully deleted.");
			break;
			
		default:
	}
	
}

/**
 * Filter submit handler
 */
function yahoo_apt_mapping_datagrid_form_submit_filter($form,&$form_state) {
	//drupal_set_message('<pre>'.print_r($form_state['values']['filters'],true).'</pre>');
	
	$type = $form_state['build_info']['args'][0]; // context of data grid ie. terms or paths
	
	if($form_state['values']['op'] != 'Clear') {
	
		$_SESSION['yahoo_apt']['datagrid'][$type]['filter'] = $form_state['values']['filters'];
	
	} else {
		
		// clear filter
		if(isset($_SESSION['yahoo_apt'],$_SESSION['yahoo_apt']['datagrid'],$_SESSION['yahoo_apt']['datagrid'][$type])) {
			unset($_SESSION['yahoo_apt']['datagrid'][$type]);
		}
		
	}
	
}