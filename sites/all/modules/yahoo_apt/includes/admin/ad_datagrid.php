<?php
/*
 * ------------------------------------------------------------------------------------------------
 * Ads admin grid
 * ------------------------------------------------------------------------------------------------
 */

/**
 * List of ads
 */
function yahoo_apt_ad_datagrid_page() {
	return drupal_get_form('yahoo_apt_ad_datagrid_form');
}

/**
 * Main data grid form
 */
function yahoo_apt_ad_datagrid_form($form,&$form_state) {
	
	/**
	 * Build out query
	 */
	$query = db_select('yahoo_apt_ad', 'a')->extend('PagerDefault')->extend('TableSort');
	$query->fields('a',array('id','name'));
	$result = $query->execute();
	
	//drupal_set_message((string) $query); // @debug
	
	/**
	 * Populate rows
	 */
	$rows = array();
	while($ad=$result->fetchAssoc()) {
		$rows[$ad['id']] = array(
			'name'=> array(
				'data'=> array(
					'#markup'=> $ad['name']
				)
			),
			'operations'=> array(
				'data'=> array(
					'#type'=> 'link',
					'#href'=> "admin/structure/yahoo-apt/ads/{$ad['id']}/edit",
					'#title'=> 'Edit'
				)
			)
		);
	}
	
	$form['ads'] = array(
		'#type'=> 'tableselect',
		'#header'=> array(
			'name'=> t('Name'),
			'operations'=> t('Operations')
		),
		'#options'=> $rows,
		'#empty'=> t('No ads available.')
	);
	
	/* pager */
	$form['pager'] = array('#markup' => theme('pager'));
	
	/**
	 * Operations
	 */
	$form['operations'] = array(
		'#type'=> 'fieldset',
		'#title'=> 'Operations',
		'#weight'=> -1,
		'#tree'=> true,
		'#collapsed'=> true,
		'#collapsible'=> true
	);
	
	$form['operations']['operation'] = array(
		'#type'=> 'select',
		'#options'=> array(
			'delete'=> t('Delete')
		),
		'#empty_option'=> '--',
		'#empty_value'=> 0
	); 
	
	$form['operations']['submit'] = array(
		'#type'=> 'submit',
		'#value'=> 'Update',
		'#submit'=> array('yahoo_apt_ad_datagrid_form_submit_operation')
	);
	
	return $form;
	
}

/**
 * Operation submit handler
 */
function yahoo_apt_ad_datagrid_form_submit_operation($form,&$form_state) {
	
	$ads = array();
	
	/**
	 * Create collection of ads to operate on.
	 */
	foreach($form_state['values']['ads'] as $id=>$v) {
		if($v != 0) {
			$ads[] = $id;
		}
	}
	
	/**
	 * Kill if nothing to delete
	 */
	if(empty($ads)) {
		return;
	}
	
	/**
	 * Run operation on ads.
	 */
	switch($form_state['values']['operations']['operation']) {
		case 'delete':
			$deleted = entity_get_controller('yahoo_apt_ad')->purgeById($ads);
			drupal_set_message("Yahoo APT Adspots have been sucessfully deleted.");
			break;
			
		default:
	}
	
}