<?php
/**
 * @file
 * mpg_date_formats.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function mpg_date_formats_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_day_number';
  $strongarm->value = 'd';
  $export['date_format_day_number'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_short_day_characters';
  $strongarm->value = 'D';
  $export['date_format_short_day_characters'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_short_month_and_day';
  $strongarm->value = 'n/j';
  $export['date_format_short_month_and_day'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_time';
  $strongarm->value = 'g:h A';
  $export['date_format_time'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_url_date';
  $strongarm->value = 'Y-m-d';
  $export['date_format_url_date'] = $strongarm;

  return $export;
}
