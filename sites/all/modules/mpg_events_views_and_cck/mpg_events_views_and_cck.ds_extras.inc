<?php
/**
 * @file
 * mpg_events_views_and_cck.ds_extras.inc
 */

/**
 * Implements hook_ds_vd_info().
 */
function mpg_events_views_and_cck_ds_vd_info() {
  $export = array();

  $ds_vd = new stdClass();
  $ds_vd->api_version = 1;
  $ds_vd->vd = 'events-page';
  $ds_vd->label = 'Events: Master (Views template)';
  $export['events-page'] = $ds_vd;

  $ds_vd = new stdClass();
  $ds_vd->api_version = 1;
  $ds_vd->vd = 'organizations-page';
  $ds_vd->label = 'Organizations: Page - List (Views template)';
  $export['organizations-page'] = $ds_vd;

  $ds_vd = new stdClass();
  $ds_vd->api_version = 1;
  $ds_vd->vd = 'taxonomy_term-page';
  $ds_vd->label = 'Taxonomy_term: Page (Views template)';
  $export['taxonomy_term-page'] = $ds_vd;

  $ds_vd = new stdClass();
  $ds_vd->api_version = 1;
  $ds_vd->vd = 'venues-page';
  $ds_vd->label = 'Venues: Page - List (Views template)';
  $export['venues-page'] = $ds_vd;

  return $export;
}
