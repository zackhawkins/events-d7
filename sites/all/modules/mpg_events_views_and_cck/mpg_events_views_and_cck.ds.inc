<?php
/**
 * @file
 * mpg_events_views_and_cck.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function mpg_events_views_and_cck_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'ds_views|events-page|default';
  $ds_fieldsetting->entity_type = 'ds_views';
  $ds_fieldsetting->bundle = 'events-page';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'vd_title_h1',
    ),
    'header' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'exposed' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'rows' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'empty' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'pager' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'footer' => array(
      'weight' => '6',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['ds_views|events-page|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'ds_views|organizations-page|default';
  $ds_fieldsetting->entity_type = 'ds_views';
  $ds_fieldsetting->bundle = 'organizations-page';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'vd_title_h1',
    ),
    'header' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'exposed' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'rows' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'empty' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'pager' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'footer' => array(
      'weight' => '6',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['ds_views|organizations-page|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'ds_views|taxonomy_term-page|default';
  $ds_fieldsetting->entity_type = 'ds_views';
  $ds_fieldsetting->bundle = 'taxonomy_term-page';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'vd_title_h1',
    ),
    'exposed' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'rows' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'empty' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'pager' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['ds_views|taxonomy_term-page|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'ds_views|venues-page|default';
  $ds_fieldsetting->entity_type = 'ds_views';
  $ds_fieldsetting->bundle = 'venues-page';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'vd_title_h1',
    ),
    'header' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'exposed' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'rows' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'empty' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'pager' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'footer' => array(
      'weight' => '6',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['ds_views|venues-page|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|event|events_upcoming_block';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'event';
  $ds_fieldsetting->view_mode = 'events_upcoming_block';
  $ds_fieldsetting->settings = array(
    'upcoming_how' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'upcoming_what_where' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'upcoming_when' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|event|events_upcoming_block'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'taxonomy_term|type|default';
  $ds_fieldsetting->entity_type = 'taxonomy_term';
  $ds_fieldsetting->bundle = 'type';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h1',
        'class' => 'venue-title',
        'ft' => array(),
      ),
    ),
  );
  $export['taxonomy_term|type|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function mpg_events_views_and_cck_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'upcoming_how';
  $ds_field->label = 'Upcoming How';
  $ds_field->field_type = 3;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array();
  $export['upcoming_how'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'upcoming_what_where';
  $ds_field->label = 'Upcoming What Where';
  $ds_field->field_type = 3;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array();
  $export['upcoming_what_where'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'upcoming_when';
  $ds_field->label = 'Upcoming When';
  $ds_field->field_type = 3;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array();
  $export['upcoming_when'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'venue_custom_address';
  $ds_field->label = 'Venue Custom Address';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'code' => array(
      'value' => '[node:field-event-address:thoroughfare] [node:field-event-address:locality] [node:field-event-address:administrative_area]

',
      'format' => 'ds_code',
    ),
    'use_token' => 1,
  );
  $export['venue_custom_address'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'venue_website_phone';
  $ds_field->label = 'Venue website + phone';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<span class="venue-website"><a href="[node:field_website]">[node:title] website</a></span> <span class="venue-phone">[node:field-phone-number]</span>',
      'format' => 'ds_code',
    ),
    'use_token' => 1,
  );
  $export['venue_website_phone'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function mpg_events_views_and_cck_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'ds_views|events-page|default';
  $ds_layout->entity_type = 'ds_views';
  $ds_layout->bundle = 'events-page';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'mothership_html5_1col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'title',
      ),
      'hgroup' => array(
        1 => 'header',
      ),
      'top' => array(
        2 => 'exposed',
      ),
      'bottom' => array(
        3 => 'rows',
        4 => 'empty',
      ),
      'footer' => array(
        5 => 'pager',
        6 => 'footer',
      ),
    ),
    'fields' => array(
      'title' => 'header',
      'header' => 'hgroup',
      'exposed' => 'top',
      'rows' => 'bottom',
      'empty' => 'bottom',
      'pager' => 'footer',
      'footer' => 'footer',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
      'header' => array(
        'side-padding' => 'side-padding',
      ),
      'top' => array(
        'side-padding' => 'side-padding',
      ),
      'bottom' => array(
        'side-padding' => 'side-padding',
        'block-upcoming-events' => 'block-upcoming-events',
      ),
      'footer' => array(
        'side-padding' => 'side-padding',
      ),
    ),
    'wrappers' => array(
      'header' => 'div',
      'hgroup' => 'div',
      'top' => 'div',
      'main' => 'div',
      'main_nowrapper' => 'div',
      'bottom' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['ds_views|events-page|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'ds_views|organizations-page|default';
  $ds_layout->entity_type = 'ds_views';
  $ds_layout->bundle = 'organizations-page';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'mothership_html5_1col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'title',
      ),
      'hgroup' => array(
        1 => 'header',
      ),
      'top' => array(
        2 => 'exposed',
      ),
      'main_nowrapper' => array(
        3 => 'empty',
        4 => 'rows',
      ),
      'bottom' => array(
        5 => 'pager',
        6 => 'footer',
      ),
    ),
    'fields' => array(
      'title' => 'header',
      'header' => 'hgroup',
      'exposed' => 'top',
      'empty' => 'main_nowrapper',
      'rows' => 'main_nowrapper',
      'pager' => 'bottom',
      'footer' => 'bottom',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'header' => 'div',
      'hgroup' => 'div',
      'top' => 'div',
      'main' => 'div',
      'main_nowrapper' => 'div',
      'bottom' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['ds_views|organizations-page|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'ds_views|taxonomy_term-page|default';
  $ds_layout->entity_type = 'ds_views';
  $ds_layout->bundle = 'taxonomy_term-page';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'mothership_html5_1col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'title',
      ),
      'top' => array(
        1 => 'exposed',
      ),
      'bottom' => array(
        2 => 'empty',
        3 => 'rows',
      ),
      'footer' => array(
        4 => 'pager',
      ),
    ),
    'fields' => array(
      'title' => 'header',
      'exposed' => 'top',
      'empty' => 'bottom',
      'rows' => 'bottom',
      'pager' => 'footer',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
      'header' => array(
        'side-padding' => 'side-padding',
      ),
      'bottom' => array(
        'side-padding' => 'side-padding',
        'block-upcoming-events' => 'block-upcoming-events',
      ),
      'footer' => array(
        '' => '',
        'side-padding' => 'side-padding',
      ),
    ),
    'wrappers' => array(
      'header' => 'div',
      'hgroup' => 'div',
      'top' => 'div',
      'main' => 'div',
      'main_nowrapper' => 'div',
      'bottom' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['ds_views|taxonomy_term-page|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'ds_views|venues-page|default';
  $ds_layout->entity_type = 'ds_views';
  $ds_layout->bundle = 'venues-page';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'mothership_html5_1col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'title',
      ),
      'hgroup' => array(
        1 => 'header',
      ),
      'top' => array(
        2 => 'exposed',
      ),
      'main_nowrapper' => array(
        3 => 'empty',
        4 => 'rows',
      ),
      'bottom' => array(
        5 => 'pager',
        6 => 'footer',
      ),
    ),
    'fields' => array(
      'title' => 'header',
      'header' => 'hgroup',
      'exposed' => 'top',
      'empty' => 'main_nowrapper',
      'rows' => 'main_nowrapper',
      'pager' => 'bottom',
      'footer' => 'bottom',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'header' => 'div',
      'hgroup' => 'div',
      'top' => 'div',
      'main' => 'div',
      'main_nowrapper' => 'div',
      'bottom' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['ds_views|venues-page|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|event|events_upcoming_block';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'event';
  $ds_layout->view_mode = 'events_upcoming_block';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'upcoming_when',
        1 => 'upcoming_what_where',
        2 => 'upcoming_how',
      ),
    ),
    'fields' => array(
      'upcoming_when' => 'ds_content',
      'upcoming_what_where' => 'ds_content',
      'upcoming_how' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
  );
  $export['node|event|events_upcoming_block'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'taxonomy_term|type|default';
  $ds_layout->entity_type = 'taxonomy_term';
  $ds_layout->bundle = 'type';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['taxonomy_term|type|default'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function mpg_events_views_and_cck_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'events_upcoming_block';
  $ds_view_mode->label = 'Events Upcoming Block';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['events_upcoming_block'] = $ds_view_mode;

  return $export;
}
