<?php
/**
 * @file
 * mpg_events_feed.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function mpg_events_feed_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_datetime';
  $strongarm->value = 'Y-m-d H:i:s';
  $export['date_format_datetime'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_month_day_full_day';
  $strongarm->value = 'n/j l';
  $export['date_format_month_day_full_day'] = $strongarm;

  return $export;
}
