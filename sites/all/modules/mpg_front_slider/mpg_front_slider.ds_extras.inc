<?php
/**
 * @file
 * mpg_front_slider.ds_extras.inc
 */

/**
 * Implements hook_ds_vd_info().
 */
function mpg_front_slider_ds_vd_info() {
  $export = array();

  $ds_vd = new stdClass();
  $ds_vd->api_version = 1;
  $ds_vd->vd = 'mpg_front_slider-block';
  $ds_vd->label = 'Mpg_front_slider: Featured (Views template)';
  $export['mpg_front_slider-block'] = $ds_vd;

  return $export;
}
