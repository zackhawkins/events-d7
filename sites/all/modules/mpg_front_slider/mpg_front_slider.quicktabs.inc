<?php
/**
 * @file
 * mpg_front_slider.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function mpg_front_slider_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'mpg_front_slider';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Front Slider';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'mpg_front_slider',
      'display' => 'block',
      'args' => '',
      'title' => 'happening today',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'mpg_front_slider',
      'display' => 'block_1',
      'args' => '',
      'title' => 'happening this week',
      'weight' => '-99',
      'type' => 'view',
    ),
    2 => array(
      'vid' => 'mpg_front_slider',
      'display' => 'block_2',
      'args' => '',
      'title' => 'happening this month',
      'weight' => '-98',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'Navlist';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Front Slider');
  t('happening this month');
  t('happening this week');
  t('happening today');

  $export['mpg_front_slider'] = $quicktabs;

  return $export;
}
