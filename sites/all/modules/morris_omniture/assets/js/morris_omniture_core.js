(function($,undef) {
  $(function() {
    $('#morris-omniture-mappings-form').each(function() {
      $('input.btn-proxy').live('click',function(e) {
        e.stopPropagation();
        e.preventDefault();

        $(e.target).next().trigger('mousedown');
      });
    });
  });
})(jQuery);
