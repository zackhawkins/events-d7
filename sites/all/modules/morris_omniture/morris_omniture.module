<?php
/**
 * @file
 * The Morris Omniture module main file.
 */

/**
 * ----------------------------------------------------------------------
 * Global module constants.
 * ----------------------------------------------------------------------
 */

// Drupal variable prefix for all variables owned by this module.
define('MORRIS_OMNITURE_VAR_PREFIX', 'morris_omniture_');

// Module name reference.
define('MORRIS_OMNITURE_MOD', 'morris_omniture');

// Use these constants to reference permissions rather than
// hard-coded string names.
define('MORRIS_OMNITURE_PERM_CONF', 'admin morris omniture config');
define('MORRIS_OMNITURE_PERM_VARM', 'admin morris omniture var mappings');
define('MORRIS_OMNITURE_PERM_OVR', 'admin morris omniture page mappings');

/**
 * ----------------------------------------------------------------------
 * Externally required include files.
 * ----------------------------------------------------------------------
 */

// The var plugin base class.
require_once 'includes/var_base.php';

// The entity and repo (controllers).
require_once 'includes/entity/var.php';
require_once 'includes/entity/mapping.php';
require_once 'includes/repo/var.php';
require_once 'includes/repo/mapping.php';

// The hook implementations per module.
require_once 'includes/bridge/system.php';
require_once 'includes/bridge/user.php';
require_once 'includes/bridge/menu.php';
require_once 'includes/bridge/entity.php';
require_once 'includes/bridge/ctools.php';
require_once 'includes/bridge/context.php';
require_once 'includes/bridge/self.php';

// Form options.
require_once 'includes/options.php';

/**
 * ----------------------------------------------------------------------
 * Default global configuration helpers.
 * ----------------------------------------------------------------------
 */

/**
 * Get omniture default config.
 *
 * @return array 
 *   Omniture config.
 */
function morris_omniture_default_config() {

  $config = array();

  // Populate the config.
  foreach (morris_omniture_config_fields() as $field) {
    switch ($field) {
      case 'js_script':
        $config[$field] = drupal_get_path('module', MORRIS_OMNITURE_MOD) . '/assets/js/morris_omniture_s_code.js';
        break;

      case 'img_beacon':
        $config[$field] = '';
        break;

      default:
        $config[$field] = variable_get(MORRIS_OMNITURE_VAR_PREFIX . $field, NULL);
    }

    // Type cast integers that represent boolean values.
    if(in_array($field,array('output_enabled'))) {
        $config[$field] = (bool) ((int) $config[$field]);
    }

  }

  return $config;

}

/**
 * Omniture config fields.
 *
 * @return array 
 *   The default global options.
 */
function morris_omniture_config_fields() {
  return array(
    'account',
    'server',
    'channel',
    'js_script',
    'img_beacon',
    'link_internal_filters',
    'link_track_vars',
    'link_download_file_types',
    'output_enabled'
  );
}

/**
 * ----------------------------------------------------------------------
 * Short-hand repository access.
 * ----------------------------------------------------------------------
 */

/**
 * Loads single variable mapping by ID.
 *
 * @param int $id
 *   variable mapping ID
 * 
 * @return MorrisOmniture_Var
 *   Instance of MorrisOmniture_Var.
 */
function morris_omniture_var_load($id) {
  $vars = entity_get_controller('morris_omniture_var')->load(array($id));
  return array_pop($vars);
}

/**
 * Wrapper function to retrieve variable mappings.
 *
 * @return MorrisOmniture_Var[]
 *   Collection of variable mappings.
 */
function morris_omniture_get_props() {
  return entity_get_controller('morris_omniture_var')->load();
}

/**
 * Load a variable plugin.
 *
 * @param str $name
 *   The plugin name.
 * 
 * @return morris_omniture_ivar
 *   Plugin instance.
 */
function morris_omniture_var_plugin_load($name) {
  ctools_include('plugins');
  return ctools_get_plugins(MORRIS_OMNITURE_MOD, 'var', $name);
}

/**
 * Ctools wrapper function to retrieve omniture var plugins.
 *
 * @return morris_omniture_ivar[]
 *   All variable plugins.
 */
function morris_omniture_get_vars() {
  ctools_include('plugins');
  return ctools_get_plugins(MORRIS_OMNITURE_MOD, 'var');
}

/**
 * Get all available plugins grouped.
 *
 * @return morris_omniture_ivar[]
 *   Collection of all variable plugins grouped together.
 */
function morris_omniture_plugins_grouped() {

  // Load all var plugins.
  $plugins = morris_omniture_get_vars();

  // Collect into groups.
  $grouped = array();
  foreach ($plugins as $name => &$plugin) {
    $group = $plugin->get_group() ? $plugin->get_group() : 'ungrouped';
    $grouped[$group][$name] = $plugin;
  }

  return $grouped;

}

/**
 * Load mapping by context name.
 *
 * @param str $context
 *   The context ID/name.
 * 
 * @return MorrisOmniture_Mapping
 *   The mapping associated with provided context.
 */
function morris_omniture_mapping_load_by_context($context) {

  $conditions = array(
    'mapping' => array(
      'context_id' => (string) $context,
    ),
  );

  $mappings = entity_get_controller('morris_omniture_mapping')->load(NULL, $conditions);
  return array_pop($mappings);

}

/**
 * ----------------------------------------------------------------------
 * Mapping discovery functions
 * ----------------------------------------------------------------------
 */

/**
 * Match a single context for the current request.
 *
 * @return StdClass
 *   The first most specific context matching the page request.
 */
function morris_omniture_discover_context() {

  static $request_context;

  if ($request_context === NULL) {

    $contexts = context_active_contexts();

    if (empty($contexts)) {
      $request_context = FALSE;
      return NULL;
    }

    // precedence:
    // 4: path mapping.
    // 3: term mapping.
    // 2: Any mapping not side wide.
    // 1: Site wide mapping.
    // The path conditon which the most specific.
    foreach ($contexts as $c) {
      if (isset($c->reactions['morris_omniture_mapping']) && $c->reactions['morris_omniture_mapping']['mapping']) {
        if (isset($c->conditions['path'])) {
          $request_context = $c;
          return $request_context;
        }
      }
    }

    // Next comes term mapping.
    foreach ($contexts as $c) {
      if (isset($c->reactions['morris_omniture_mapping']) && $c->reactions['morris_omniture_mapping']['mapping']) {
        if (isset($c->conditions['node_taxonomy'])) {
          $request_context = $c;
          return $request_context;
        }
      }
    }

    // Otherwise first context that is not the side wide default.
    foreach ($contexts as $c) {
      if (isset($c->reactions['morris_omniture_mapping']) && $c->reactions['morris_omniture_mapping']['mapping']) {
        $request_context = $c;
        return $request_context;
      }
    }

    // When default context exists use that.
    if (!$request_context) {
      $request_context = FALSE;
    }

  }

  if ($request_context === FALSE) {
    return NULL;
  }
  else {
    return $request_context;
  }

}

/**
 * Get mapping for current request.
 *
 * @return MorrisOmniture_Mapping
 *   Mapping for the current request.
 */
function morris_omniture_discover_mapping() {

  static $request_mapping;

  if ($request_mapping === NULL) {

    $context = morris_omniture_discover_context();

    if ($context) {

      $request_mapping = morris_omniture_mapping_load_by_context($context->name);

    }

    // When no mapping exists use default.
    if (!$request_mapping) {
      $request_mapping = FALSE;
    }

  }

  if ($request_mapping) {
    return $request_mapping;
  }
  else {
    return NULL;
  }

}

/**
 * ----------------------------------------------------------------------
 * Omniture sitecatalyst data structure builders.
 * ----------------------------------------------------------------------
 */

/**
 * Build omniture variable array.
 *
 * @return array
 *   Omniture variable output for current page request.
 */
function morris_omniture_build_vars() {

  $params = array();

  // Get default/global configuration.
  $config = morris_omniture_default_config();

  // Apply the globals.
  $params['server'] = $config['server'];
  $params['channel'] = $config['channel'];

  // Get the mapping.
  $mapping = morris_omniture_discover_mapping();

  // Apply channel override when one exists.
  if ($mapping && $mapping->get('morris.channel')) {
    $params['channel'] = $mapping->get('morris.channel');
  }

  // Derive value for each mapped plugin.
  $props = array();
  foreach (morris_omniture_get_props() as $var) {
    $props[$var->getId()] = $var->getPlugin()->val();
  }
  
  // Get the prop keys.
  $prop_keys = array_keys($props);
  
  // Sort the property keys with natural algorithm.
  natsort($prop_keys);
  
  // Merge with params properly sorted.
  foreach($prop_keys as $key) {
      $params[$key] = $props[$key];
  }
  
  unset($props,$prop_keys);

  return $params;

}

/**
 * ----------------------------------------------------------------------
 * The misc utility helpers.
 * ----------------------------------------------------------------------
 */

/**
 * Apply omniture tracking to page.
 */
function morris_omniture_apply_tracking() {

  // Get the config.
  $config = morris_omniture_default_config();

  // Get build object.
  $vars = morris_omniture_build_vars();

  // Convert vars build object to js.
  $js = morris_omniture_convert_params_to_js($vars);

  // The adobe Sitecatalyst s_code options.
  drupal_add_js(array(
    'morris_omniture' => array(
      'account' => $config['account'],
      'link_internal_filters' => $config['link_internal_filters'],
      'link_track_vars' => $config['link_track_vars'],
      'link_download_file_types' => $config['link_download_file_types'],
    ),
  ), array('type' => 'setting'));

  // Add external script.
  drupal_add_js($config['js_script'], array('type' => 'file', 'scope' => 'footer'));

  // Add the vars.
  drupal_add_js($js, array('type' => 'inline', 'scope' => 'footer'));

  // End code snippet.
  drupal_add_js("var s_code=s.t();if(s_code)document.write(s_code);if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-');", array('type' => 'inline', 'scope' => 'footer'));

  // Image beacon optional.
  if (!empty($config['img_beacon'])) {
  }

}

/**
 * Apply request debugging info.
 * 
 * When debugging is on add additional infromation to output to ease
 * process of determing why expected mapping settings are not being
 * applied. The main reason this would occur is because the mapping
 * was specified incorrectly or mutiple mappings exist where the most
 * specific one is chosen based on the rules defined by the mapping
 * disocovery function.
 */
function morris_omniture_apply_debug_info() {

  // The mapping used for request.
  $mapping = morris_omniture_discover_mapping();

  // The context used for request.
  $context = morris_omniture_discover_context();

  // All active contexts for request.
  $contexts = array();
  foreach (context_active_contexts() as $c) {
    $contexts[] = $context && $c->name == $context->name ? "<b>{$c->name}</b>" : $c->name;
  }

  $id = $mapping ? $mapping->getId() : 'NULL';

  drupal_set_message("Mapping ID: $id");
  drupal_set_message('Contexts: ' . implode(',', $contexts));
  drupal_set_message('Tracking Disabled: ' . (morris_omniture_disable_tracking() ? '<b>Yes</b>' : 'No'));

  // The Omniture data.
  $config = morris_omniture_default_config();

  // Get build object.
  $vars = morris_omniture_build_vars();

  // Convert vars build object to js.
  $js = morris_omniture_convert_params_to_js($vars, 'debug');

  // Log to console - KISS!
  drupal_add_js("
        (function() {
            var debug = {};
            $js
            if(console || console.log) {
                console.log('Omniture Sitecatalyst data:');
                console.log(debug);
            }
        })();
    ", array('type' => 'inline', 'scope' => 'footer'));

}

/**
 * Convert JS build object/array into pure JS.
 *
 * @param array $params
 *   The Omniture variable values.  
 * 
 * @param str $prefix
 *   Prefix for the variable name.
 * 
 * @return str 
 *   The javascript code snippet.
 */
function morris_omniture_convert_params_to_js($params, $prefix = 's') {

  $js = '';

  foreach ($params as $name => $value) {
    if (is_null($value)) {
      continue;
    }
    elseif (is_array($value) && !empty($value)) {
      $js .= "$prefix.$name = ['" . implode("','", $value) . "'];" . PHP_EOL;
    }
    elseif (is_int($value)) {
      $js .= "$prefix.$name = $value;" . PHP_EOL;
    }
    elseif (is_object($value) && strcasecmp($value->type, 'js') === 0) {
      $js .= "$prefix.$name = " . $value->raw . PHP_EOL;
    }
    elseif (!is_array($value)) {
      $js .= "$prefix.$name = '$value';" . PHP_EOL;
    }
  }

  return $js;

}

/**
 * Safely wrap raw javaScript code to be output as omniture property.
 * 
 * Embed pure JS code on page using plugins via passing through
 * this function. When this is done the converter knows to
 * embed data as is.
 *
 * @param str $js
 *   A JavaScript code snippet.
 * 
 * @return StdClass
 *   Wrapper for JavaScript code.
 */
function morris_omniture_js_snippet_wrapper($js) {

  $wrapper = new StdClass();
  $wrapper->raw = $js;
  $wrapper->type = 'js';
  return $wrapper;

}

/**
 * Determine whether user can add and remove variable mappings. 
 * 
 * Only users who understand the business implications of doing this
 * should have this permission. Otherwise, things can possibly get
 * real messed up from a business perspective.
 *
 * @return bool
 *   Allow users to add and delete variable mappings.
 */
function morris_omniture_user_var_mapping_admin() {
  return user_access(MORRIS_OMNITURE_PERM_VARM);
}

/**
 * Generate new unique mapping name. 
 * 
 * This will be used as the context name when creating
 * a new context through the module add path
 * and term pages which essentially wrap around the 
 * standard context form.
 *
 * @return str
 *   Generate unique mapping name string.
 */
function morris_omniture_generate_unique_mapping_name() {
  return uniqid('morris_omniture_mapping_');
}

/**
 * Determine whether tracking should be disabled for the current request.
 *
 * @return bool
 *   Whether to disable tracking for current page request.
 */
function morris_omniture_disable_tracking() {

  $p = $_GET['q'];
  
  /*
   * Get configuration to check whether tacking
   * has been explicitly turned-off.
   */
  $config = morris_omniture_default_config();
  
  /*
   * Site catalyst tagging can be turned off using a mapping.
   */ 
  $mapping = morris_omniture_discover_mapping();
  if($mapping && $mapping->get('tracking_disabled') === true) {
      return true;
  }

  /*
   * Disable all forms of tracking on admin dashboard, all
   * admin sub pages and advanced help.
   */
  return !$config['output_enabled'] || strpos($p, 'admin/') === 0 || strcmp($p, 'admin') === 0 || strpos($p, 'help/') === 0;

}

/**
 * Determine whether debugging request state should be on.
 *
 * @return bool
 *   Whether to turn on debugging or not.
 */
function morris_omniture_debug_mode() {
  return isset($_GET['morris_omniture_debug']);
}
