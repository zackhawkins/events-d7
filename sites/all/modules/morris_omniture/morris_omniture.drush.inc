<?php
/**
 * @file
 * Drush commands and callbacks.
 */

/**
 * Implements hook_drush_command().
 */
function morris_omniture_drush_command() {

  $items = array();

  /*
   * Install default variable mappings for morris business unit.
   */
  $items['morris-omniture-morris-var-mappings-install'] = array(
    'description' => 'Install default variable mappings for morris business units.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
    'aliases' => array('mo-vmi'),
  );

  return $items;

}

/**
 * ------------------------------------------------
 */

/**
 * Install default data.
 */
function drush_morris_omniture_morris_var_mappings_install() {

  // The morris mappings.
  $vars = array();
  $vars[] = array(
    "id" => "pageName",
    "plugin" => "morris_page_name",
    "grp" => NULL,
    "weight" => 0,
  );
  $vars[] = array(
    "id" => "pageType",
    "plugin" => "morris_page_type",
    "grp" => NULL,
    "weight" => 0,
  );
  $vars[] = array(
    "id" => "prop1",
    "plugin" => "morris_path",
    "grp" => "Page",
    "weight" => 0,
  );
  $vars[] = array(
    "id" => "prop14",
    "plugin" => "user_name",
    "grp" => "Corporate",
    "weight" => 0,
  );
  $vars[] = array(
    "id" => "prop15",
    "plugin" => "morris_region",
    "grp" => "Corporate",
    "weight" => 0,
  );
  $vars[] = array(
    "id" => "prop16",
    "plugin" => "morris_group",
    "grp" => "Corporate",
    "weight" => 0,
  );
  $vars[] = array(
    "id" => "prop17",
    "plugin" => "morris_category",
    "grp" => "Corporate",
    "weight" => 0,
  );
  $vars[] = array(
    "id" => "prop18",
    "plugin" => "morris_subcategory",
    "grp" => "Corporate",
    "weight" => 0,
  );
  $vars[] = array(
    "id" => "prop19",
    "plugin" => "yahoo_apt_ads",
    "grp" => "Corporate",
    "weight" => 0,
  );
  $vars[] = array(
    "id" => "prop2",
    "plugin" => "doc_title",
    "grp" => "Page",
    "weight" => 0,
  );
  $vars[] = array(
    "id" => "prop21",
    "plugin" => "node_id",
    "grp" => "Node",
    "weight" => 0,
  );
  $vars[] = array(
    "id" => "prop24",
    "plugin" => "node_bundle",
    "grp" => "Node",
    "weight" => 0,
  );

  // Start the transaction.
  $transaction = db_transaction();

  try {

    // Build the query.
    $query = db_insert('morris_omniture_var');
    $query->fields(array('id', 'plugin', 'grp', 'weight'));
    foreach ($vars as $var) {
      $query->values($var);
    }

    // Insert the data.
    $query->execute();

    // The default link tracking variables.
    variable_set('morris_omniture_link_track_vars', 'eVar1,prop21,prop22,prop23,prop24');
    variable_set('morris_omniture_link_download_file_types', 'exe,zip,wav,mp3,mov,mpg,avi,wmv,doc,pdf,xls');

    // Write success message to console.
    drush_print('Morris omniture variable mappings sucessfully created.');

    // Additional configuration necessary but unable to automate.
    drush_print('To complete configuration for Morris customers please set the account, server and channel fields in the configuration area.');
    drush_print('Additionally, the group, region, category, subcategory and pageType mappings expect default settings to be specified. Otherwise these properties will pass as blank values to Omniture.');

  }
  catch (Exception $e) {

    // Rollback the transaction.
    $transaction->rollback();

    // Set the drush error.
    drush_set_error('MORRIS_OMNITURE_ERROR', 'An error has occured attempting to insert default morris variable mapping data.');

    // Command failure expected to return false.
    return FALSE;
  }

}
