<?php
/**
 * @file
 * The module hook API.
 */

/**
 * Modify mapping load query.
 *
 * @param object $query
 *   The query object.
 * 
 * @param array $ids
 *   The entity IDs.
 * 
 * @param array $conditions
 *   Additional query conditions.
 */
function morris_omniture_morris_omniture_mapping_load_query_alter($query, $ids, $conditions) {
}

/**
 * Persist additional data associated with mapping.
 *
 * @param MorrisOmniture_Mapping $mapping
 *   The mapping data container.
 */
function morris_omniture_morris_omniture_mapping_persist($mapping) {

  $data = array(
    'category' => $mapping->get('morris.category'),
    'subcategory' => $mapping->get('morris.subcategory'),
    'page_name' => $mapping->get('morris.page_name'),
  );

  db_update('morris_omniture_mapping')->fields($data)->condition('id', $mapping->getId())->execute();

}

/**
 * Overload mapping with custom meta data.
 *
 * @param MorrisOmniture_Mapping $mapping
 *   The mapping data container. 
 * 
 * @param array $row
 *   The raw query result row.
 */
function morris_omniture_morris_omniture_mapping_map_select_result($mapping, $row) {

  $mapping->set('morris.category', $row['category']);
  $mapping->set('morris.subcategory', $row['subcategory']);
  $mapping->set('morris.page_name', $row['page_name']);

}

/**
 * React to mapping delete.
 *
 * @param StdClass $context
 *   The context associated with mapping that is getting deleted.
 */
function morris_omniture_morris_omniture_mapping_purge($context) {
}
