<?php
/**
 * @file
 * Contains morris form field options.
 */

/**
 * The morris regions options.
 *
 * @return array
 *   Morris region options.
 */
function _morris_omniture_region_options() {
  return array(
    '' => t('None'),
    'AK' => t('AK'),
    'FL' => t('FL'),
    'MW' => t('MW'),
    'SE' => t('SE'),
  );
}

/**
 * The morris group options.
 *
 * @return array
 *   Morris group options.
 */
function _morris_omniture_group_options() {
  return array(
    '' => t('None'),
    'Eastern' => t('Eastern'),
    'Florida' => t('Florida'),
    'Metro' => t('Metro'),
    'Western' => t('Western'),
  );
}

/**
 * The morris category options.
 *
 * @return array
 *   Morris category options.
 */
function _morris_omniture_category_options() {
  return array(
    '' => t('None'),
    'Activote' => t('Activote'),
    'Business News' => t('Business News'),
    'Community Agate' => t('Community Agate'),
    'Employment' => t('Employment'),
    'Entertainment' => t('Entertainment'),
    'Home' => t('Home'),
    'Lifestyle' => t('Lifestyle'),
    'Marketplace' => t('Marketplace'),
    'Mobile' => t('Mobile'),
    'News' => t('News'),
    'Opinions' => t('Opinions'),
    'Other' => t('Other'),
    'Police Blotter'=> t('Police Blotter'),
    'Real Estate' => t('Real Estate'),
    'Search' => t('Search'),
    'Sports News' => t('Sports News'),
    'Transportation' => t('Transportation'),
    'UCC' => t('UCC'),
    'Video' => t('Video'),
  );
}

/**
 * MDW sub category options.
 *
 * @return array
 *   Morris sub category options.
 */
function _morris_omniture_subcategory_options() {
  return array(
    '' => t('None'),
    '97010 ElectionsCandidates' => t('97010 ElectionsCandidates'),
    '97010 ElectionsBlogs' => t('97010 ElectionsBlogs'),
    '97010 ElectionsForums' => t('97010 ElectionsForums'),
    '97010 ElectionsIssues' => t('97010 ElectionsIssues'),
    '97010 ElectionsCivics101' => t('97010 ElectionsCivics101'),
    '97010 ElectionsBallot' => t('97010 ElectionsBallot'),
    '97010 ElectionsvHarmony' => t('97010 ElectionsvHarmony'),
    '97010 ElectionsProfile' => t('97010 ElectionsProfile'),
    '97010 ElectionsCandidates' => t('97010 ElectionsCandidates'),
    '97010 Archives' => t('97010 Archives'),
    '97010 AllBusiness' => t('97010 AllBusiness'),
    '97010 BusinessFinancialMarkets' => t('97010 BusinessFinancialMarkets'),
    '97010 BusinessGeneral' => t('97010 BusinessGeneral'),
    '97010 BusinessPersonal' => t('97010 BusinessPersonal'),
    '97010 AllCommunityAgate' => t('97010 AllCommunityAgate'),
    '97010 CABirths' => t('97010 CABirths'),
    '97010 CACelebrations' => t('97010 CACelebrations'),
    '97010 CAEngagements' => t('97010 CAEngagements'),
    '97010 CAObituaries' => t('97010 CAObituaries'),
    '97010 CAPolice' => t('97010 CAPolice'),
    '97010 CAWeddings' => t('97010 CAWeddings'),
    '97020 AllEmployment' => t('97020 AllEmployment'),
    '97020 EmploymentClassifieds' => t('97020 EmploymentClassifieds'),
    '97020 EmploymentContent' => t('97020 EmploymentContent'),
    '97020 EmploymentInventory' => t('97020 EmploymentInventory'),
    '97020 EmploymentReviews' => t('97020 EmploymentReviews'),
    '97040 AllEntertainment' => t('97040 AllEntertainment'),
    '97040 EntertainmentCalendar' => t('97040 EntertainmentCalendar'),
    '97040 EntertainmentEvents' => t('97040 EntertainmentEvents'),
    '97040 EntertainmentMovies' => t('97040 EntertainmentMovies'),
    '97040 EntertainmentMusic' => t('97040 EntertainmentMusic'),
    '97040 EntertainmentReviews' => t('97040 EntertainmentReviews'),
    '97040 EntertainmentTV' => t('97040 EntertainmentTV'),
    '97010 Home' => t('97010 Home'),
    '97010 AllLifestyle' => t('97010 AllLifestyle'),
    '97010 LifestyleFood' => t('97010 LifestyleFood'),
    '97010 LifestylePeople' => t('97010 LifestylePeople'),
    '97010 LifestyleReligion' => t('97010 LifestyleReligion'),
    '97010 LifestyleTechnology' => t('97010 LifestyleTechnology'),
    '97010 LifestyleTravel' => t('97010 LifestyleTravel'),
    '97020 AllMarketplace' => t('97020 AllMarketplace'),
    '97020 MarketplaceAnnouncements' => t('97020 MarketplaceAnnouncements'),
    '97020 MarketplaceBusinessDirectory' => t('97020 MarketplaceBusinessDirectory'),
    '97020 MarketplaceCareerbuilder' => t('97020 MarketplaceCareerbuilder'),
    '97020 MarketplaceGarageSales' => t('97020 MarketplaceGarageSales'),
    '97020 MarketplaceLegals' => t('97020 MarketplaceLegals'),
    '97020 MarketplaceMerchandise' => t('97020 MarketplaceMerchandise'),
    '97020 MarketplacePets' => t('97020 MarketplacePets'),
    '97020 MarketplaceServices' => t('97020 MarketplaceServices'),
    '97020 MarketplaceTopJobs' => t('97020 MarketplaceTopJobs'),
    '97050 AllMobile' => t('97050 AllMobile'),
    '97050 MobileBlogs' => t('97050 MobileBlogs'),
    '97050 MobileClassifieds' => t('97050 MobileClassifieds'),
    '97050 MobileMovies' => t('97050 MobileMovies'),
    '97050 MobileNews' => t('97050 MobileNews'),
    '97050 MobileSpotted' => t('97050 MobileSpotted'),
    '97050 MobileYellowAdvantage' => t('97050 MobileYellowAdvantage'),
    '97010 AllNews' => t('97010 AllNews'),
    '97010 NewsAP' => t('97010 NewsAP'),
    '97010 NewsLegals' => t('97010 NewsLegals'),
    '97010 NewsLocal' => t('97010 NewsLocal'),
    '97010 NewsNonLocal' => t('97010 NewsNonLocal'),
    '97010 NewsState' => t('97010 NewsState'),
    '97010 NewsWeather' => t('97010 NewsWeather'),
    '97010 AllOpinions' => t('97010 AllOpinions'),
    '97010 OpinionsColumnists' => t('97010 OpinionsColumnists'),
    '97010 OpinionsEditorials' => t('97010 OpinionsEditorials'),
    '97010 OpinionsLetters' => t('97010 OpinionsLetters'),
    '97040 Other' => t('97040 Other'),
    '97020 AllRE' => t('97020 AllRE'),
    '97020 REClassifieds' => t('97020 REClassifieds'),
    '97020 REContent' => t('97020 REContent'),
    '97020 REInventory' => t('97020 REInventory'),
    '97020 RENews' => t('97020 RENews'),
    '97010 AllSports' => t('97010 AllSports'),
    '97010 SportsCollege' => t('97010 SportsCollege'),
    '97010 SportsLocal' => t('97010 SportsLocal'),
    '97010 SportsOther' => t('97010 SportsOther'),
    '97010 SportsOutdoor' => t('97010 SportsOutdoor'),
    '97010 SportsProfessional' => t('97010 SportsProfessional'),
    '97020 AllAutos' => t('97020 AllAutos'),
    '97020 AutosContent' => t('97020 AutosContent'),
    '97020 AutosInventory' => t('97020 AutosInventory'),
    '97020 AutosReviews' => t('97020 AutosReviews'),
    '97020 AutosSearch' => t('97020 AutosSearch'),
    '97030 AllUCC' => t('97030 AllUCC'),
    '97030 UCCBlogs' => t('97030 UCCBlogs'),
    '97030 UCCBlogsCommunity' => t('97030 UCCBlogsCommunity'),
    '97030 UCCBlogsExpert' => t('97030 UCCBlogsExpert'),
    '97030 UCCBlogsNewsroom' => t('97030 UCCBlogsNewsroom'),
    '97030 UCCGroups' => t('97030 UCCGroups'),
    '97030 UCCMessageBoards' => t('97030 UCCMessageBoards'),
    '97030 UCCPolls' => t('97030 UCCPolls'),
    '97030 UCCProfiles' => t('97030 UCCProfiles'),
    '97030 UCCRatings' => t('97030 UCCRatings'),
    '97030 UCCSpotted' => t('97030 UCCSpotted'),
    '97030 NewsVideo' => t('97030 NewsVideo'),
    '97030 UCCVideo' => t('97030 UCCVideo'),
    '97040 Search' => t('97040 Search'),
    '97040 SearchImage' => t('97040 SearchImage'),
    '97040 SearchAuto' => t('97040 SearchAuto'),
  );
}
