<?php
/**
 * @file
 * Morris omniture base class (template pattern).
 */

require_once 'ivar.php';

/**
 * Base template class.
 */
abstract class morris_omniture_var_base implements morris_omniture_ivar {

  protected $plugin;

  /**
   * Class constructor.
   * 
   * @param array $plugin
   *   The plugin data.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * Default implementation to get name of plugin.
   * 
   * @return str
   *   The name of the plugin.
   */
  public function get_name() {
    return $this->plugin['name'];
  }

  /**
   * Default implementation of get plugin group.
   * 
   * @return str
   *   The group which plugin is associated.
   */
  public function get_group() {
    return $this->plugin['group'];
  }

  /**
   * Default implementation of plugin title.
   * 
   * This will be the title that is shown to end users
   * to identify the plugin.
   * 
   * @return str
   *   Title used to identify plugin shoqn on admin UI.
   */
  public function get_title() {
    return $this->plugin['title'];
  }

  /**
   * Default implementation of description for plugin.
   * 
   * This will be the end-user description of the plugin.
   * It should make sense to the average Drupal user.
   * 
   * @return str
   *   The plugin UI description.
   */
  public function get_description() {
    return $this->plugin['description'];
  }

  /**
   * The plugin UI title.
   * 
   * @return str
   *   The UI title.
   */
  public function ui_title() {
    $group = $this->get_group();

    if (empty($group)) {
      return $this->get_title();
    }
    else {
      return sprintf('%s->%s', $this->get_group(), $this->get_title());
    }

  }

  /**
   * Whether the plugin values can be overriden per request.
   * 
   * @return bool
   *   Plugin values can be overriden.
   */
  public function overridable() {
    return $this->plugin['overridable'];
  }

  /**
   * Optional settings for plugin.
   * 
   * These settings will be exposed as a form when mapping
   * a plugin.
   * 
   * @param array &$form_state
   *   The form state array.
   * 
   * @return array
   *   The settings form builder partial.
   */
  public function settings(&$form_state) {
    $settings = array();
    return $settings;
  }

  /**
   * Settings submit handler.
   * 
   * @param array $values
   *   The submitted values.
   */
  public function settings_submit($values) {
  }

  /**
   * Contextual settings form.
   * 
   * @param StdClass $context
   *   Context to bind settings to.
   * 
   * @return array
   *   The additional form elements.
   */
  public function override_form($context) {
  }

  /**
   * Submit handler to contextual settings form.
   * 
   * @param array $values
   *   The submitted values.
   */
  public function override_submit($values) {
  }

  /**
   * Function called when variable is unmapped.
   * 
   * This method should do any necessary clean-up tasks
   * like deleting global variables or rows in tables that
   * contain plugin state.
   */
  public function unmap() {
  }

  /**
   * The end value sent to Omniture/injected into html.
   * 
   * @return mix
   *   Value sent to Omniture.
   */
  public function val() {
    return '';
  }

  /**
   * Fetch mapping data container from context.
   *
   * @param StdClass $context
   *   Context associated with plugin.
   * 
   * @return MorrisOmniture_Mapping
   *   The variable mapping.
   */
  protected function fetch_mapping_from_context($context) {
    $mapping = NULL;

    if ($context->name) {
      $mapping = morris_omniture_mapping_load_by_context($context->name);
    }

    if (!$mapping) {
      $mapping = new MorrisOmniture_Mapping();
    }

    return $mapping;

  }

  /**
   * Get mapping data container for current request.
   *
   * @return MorrisOmniture_Mapping
   *   The mapping.
   */
  protected function fetch_request_mapping() {

    $mapping = morris_omniture_mapping_form_persist_mapping();

    if (!$mapping) {
      $mapping = new MorrisOmniture_Mapping();
      morris_omniture_mapping_form_persist_mapping($mapping);
    }

    return $mapping;
  }

}
