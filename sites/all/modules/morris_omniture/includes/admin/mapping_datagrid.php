<?php
/**
 * @file
 * The mappings admin data grid.
 */

/**
 * The list of mappings.
 *
 * @param str $type
 *   The type of mapping.
 * 
 * @return array
 *   The form build array.
 */
function morris_omniture_mapping_datagrid_page($type) {

  $form_state = array();
  $form_state['build_info']['args'] = array($type);

  // Rebuild filter state from session data.
  switch ($type) {
    case 'terms':
      // Assign form state for filter from session.
      $form_state['values']['filters'] = array(
        'term' => isset($_SESSION['morris_omniture'], $_SESSION['morris_omniture']['datagrid'], $_SESSION['morris_omniture']['datagrid'][$type]) ? $_SESSION['morris_omniture']['datagrid'][$type]['filter']['term'] : NULL,
      );
      break;

    case 'paths':
    default:
      // Assign form state for filter from session.
      $form_state['values']['filters'] = array(
        'path' => isset($_SESSION['morris_omniture'], $_SESSION['morris_omniture']['datagrid'], $_SESSION['morris_omniture']['datagrid'][$type]) ? $_SESSION['morris_omniture']['datagrid'][$type]['filter']['path'] : NULL,
      );
  }

  // Build out the form.
  return drupal_build_form('morris_omniture_mapping_datagrid_form', $form_state);
}

/**
 * The mapping data grid form.
 * 
 * @param array $form
 *   The form build array.
 * 
 * @param array $form_state
 *   The form state.
 * 
 * @param str $type
 *   The type of mapping.
 */
function morris_omniture_mapping_datagrid_form($form, &$form_state, $type) {

  // Load mapping data.
  $query = db_select('morris_omniture_mapping', 'm')->extend('PagerDefault')->extend('TableSort');
  $query->innerJoin('context', 'c', 'm.context_id = %alias.name');

  $query->fields('m', array('id', 'context_id'));

  // Serialized data... yuck.
  $query->addField('c', 'conditions', 'context_conditions');

  // Filter based on mapping type and change out label for data grid.
  switch ($type) {
    case 'terms':
      $query->condition('c.tag', 'morris_omniture_term_mapping');
      $headers = array(
        'tid' => t('tid'),
        'vocab' => t('Vocab'),
        'term' => t('Term'),
        'operations' => t('Operations'),
      );

      /*
       * Apply filters
       * This is really a terrible way to filter but given
       * what we have to work with their really aren't to
       * many options. This really sucks... I hate
       * serialized data.. idk what to do here...
       * short of creating a column for tid within the
       * yahoo_apt_mapping table. Screw it for now.
       */
      if (!empty($form_state['values']['filters']['term'])) {
      }

      break;

    case 'paths':
    default:
      $query->condition('c.tag', 'morris_omniture_path_mapping');
      $headers = array(
        'path' => t('Path'),
        'operations' => t('Operations'),
      );

      /*
       * Apply filters
       * The best we can really do here due to the serialized
       * nature of the data is search against the entire
       * value using like. Not much of a better way to do
       * this without storing the path separately in the
       * mapping table which I think is best avoided.
       * Nature of using other peopels stupid decisions
       * like serializing data and stuffing it into a single column...
       */
      if (!empty($form_state['values']['filters']['path'])) {
        $query->condition("c.conditions", '%' . $form_state['values']['filters']['path'] . '%', 'LIKE');
      }

      break;
  }

  $result = $query->execute();

  // Collect row data.
  $rows = array();
  while ($mapping = $result->fetchAssoc()) {

    // Unserialize mapping conditions.
    $conditions = unserialize($mapping['context_conditions']);

    $rows[$mapping['id']] = array(
      'info' => array(
        'data' => array(
          '#markup' => '',
        ),
      ),
      'operations' => array(
        'data' => array(
          '#type' => 'link',
          '#href' => "admin/structure/morris-omniture/mappings/{$mapping['context_id']}/edit",
          '#title' => 'Edit',
        ),
      ),
    );

    switch ($type) {

      case 'terms':
        // For the time being only recognize the first term.
        $tid = array_shift($conditions['node_taxonomy']['values']);

        // Get term ancestory including self.
        $terms = array_reverse(taxonomy_get_parents_all($tid), FALSE);

        $info = '';

        // Build out label based on ancestory path.
        foreach ($terms as $index => $term) {
          if (strlen($info) !== 0) {
            $info .= ' &gt; ';
          }
          $info .= check_plain($term->name);
        }

        // The term tid.
        $rows[$mapping['id']]['tid']['data']['#markup'] = $terms[count($terms) - 1]->tid;

        // The term vocabulary.
        $rows[$mapping['id']]['vocab']['data']['#markup'] = check_plain($terms[count($terms) - 1]->vocabulary_machine_name);

        // The term path.
        $rows[$mapping['id']]['term']['data']['#markup'] = $info;
        break;

      case 'paths':
      default:
        /*
         * For the time being only display the first path
         * though mutiples are possible.
         */
        $path = array_shift($conditions['path']['values']);
        
        $rows[$mapping['id']]['path']['data']['#markup'] = check_plain($path);
        unset($path);
    }

    $rows[$mapping['id']]['operations'] = array(
      'data' => array(
        '#type' => 'link',
        '#href' => "admin/structure/morris-omniture/mappings/$type/{$mapping['context_id']}/edit",
        '#title' => 'Edit',
      ),
    );

  }

  // Build out form.
  $form = array();

  $form['mappings'] = array(
    '#type' => 'tableselect',
    '#header' => $headers,
    '#options' => $rows,
    '#empty' => t('No mappings available.'),
  );

  // Set the pager.
  $form['pager'] = array('#markup' => theme('pager'));

  // Declare the operations.
  $form['operations'] = array(
    '#type' => 'fieldset',
    '#title' => 'Operations',
    '#weight' => -1,
    '#tree' => TRUE,
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  );

  $form['operations']['operation'] = array(
    '#type' => 'select',
    '#options' => array(
      'delete' => t('Delete'),
    ),
    '#empty_option' => '--',
    '#empty_value' => 0,
  );

  $form['operations']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Update',
    '#submit' => array('morris_omniture_mapping_datagrid_form_submit_operation'),
  );

  // Filter the form.
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => 'Search',
    '#weight' => -2,
    '#tree' => TRUE,
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  );

  $form['filters']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Search',
    '#submit' => array('morris_omniture_mapping_datagrid_form_submit_filter'),
    '#weight' => 100,
  );

  $form['filters']['clear'] = array(
    '#type' => 'submit',
    '#value' => 'Clear',
    '#submit' => array('morris_omniture_mapping_datagrid_form_submit_filter'),
    '#weight' => 101,
  );

  switch ($type) {
    case 'terms':
      $form['filters']['term'] = array(
        '#type' => 'textfield',
        '#title' => 'Term ID',
        '#default_value' => $form_state['values']['filters']['term'],
      );

      // Kill for now - not functioning - serialized data makes me want to cry.
      $form['filters']['#access'] = FALSE;

      break;

    case 'paths':
    default:
      $form['filters']['path'] = array(
        '#type' => 'textfield',
        '#title' => 'Path',
        '#default_value' => $form_state['values']['filters']['path'],
      );
  }

  return $form;

}

/**
 * The operation submit handler.
 * 
 * @param array $form
 *   The form build array.
 * 
 * @param array $form_state
 *   The form state.
 */
function morris_omniture_mapping_datagrid_form_submit_operation($form, &$form_state) {

  $mappings = array();

  // Create collection of mappings to operate on.
  foreach ($form_state['values']['mappings'] as $id => $v) {
    if ($v != 0) {
      $mappings[] = $id;
    }
  }

  // Kill if nothing to delete.
  if (empty($mappings)) {
    return;
  }

  // Run operation on mappings.
  switch ($form_state['values']['operations']['operation']) {
    case 'delete':
      $deleted = entity_get_controller('morris_omniture_mapping')->purgeById($mappings);
      drupal_set_message("Morris Omniture mappings have been sucessfully deleted.");
      break;

    default:
  }

}

/**
 * Filter submit handler.
 * 
 * @param array $form
 *   The form build array.
 * 
 * @param array $form_state
 *   The form state.
 */
function morris_omniture_mapping_datagrid_form_submit_filter($form, &$form_state) {

  // The context of data grid ie. terms or paths.
  $type = $form_state['build_info']['args'][0];

  if ($form_state['values']['op'] != 'Clear') {

    $_SESSION['morris_omniture']['datagrid'][$type]['filter'] = $form_state['values']['filters'];

  }
  else {

    // Clear the filter.
    if (isset($_SESSION['morris_omniture'], $_SESSION['morris_omniture']['datagrid'], $_SESSION['morris_omniture']['datagrid'][$type])) {
      unset($_SESSION['morris_omniture']['datagrid'][$type]);
    }

  }

}
