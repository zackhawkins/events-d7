<?php
/**
 * @file
 * The variable mapping repository.
 */

class MorrisOmniture_Var_Repository implements DrupalEntityControllerInterface {

  /**
   * Class constructor.
   * 
   * @param str $entity_type
   *   The entity type.
   */
  public function __construct($entity_type) {
  }

  /**
   * Reset internal entity cache.
   * 
   * @param array $ids
   *   Collection of entity IDs.
   */
  public function resetCache(array $ids = NULL) {
  }

  /**
   * Load variable mappings.
   *
   * @param str[] $ids
   *   Collection of primary keys.
   * 
   * @param array $conditions
   *   Various conditions to apply when loading variables.
   * 
   * @return MorrisOmniture_Var[]
   *   Collection of MorrisOmniture_Var instances.
   */
  public function load($ids = array(), $conditions = array()) {

    $query = $this->_makeBaseSelectQuery($conditions);

    $query->addTag('morris_omniture_var_load');

    if (!empty($ids)) {
      $query->condition('v.id', $ids);
    }

    $result = $query->execute();

    $mappings = $this->_mapSelectResult($result);

    return $mappings;

  }

  /**
   * Save mapping to persistent storage mechanism.
   *
   * @param MorrisOmniture_Var $var
   *   The variable object to persist to the db.
   */
  public function persist(MorrisOmniture_Var $var) {

    $transaction = db_transaction();

    try {

      $data = array(
        'id' => $var->getId(),
        'plugin' => $var->getPlugin()->get_name(),
        'grp' => $var->getGrp(),
        'weight' => $var->getWeight(),
      );

      /*
       * Check to see whether we need to insert or update.
       */
      $exists = !!($this->load(array($var->getId())));

      if ($exists) {

        db_update('morris_omniture_var')->fields($data)->condition('id', $var->getId())->execute();

      }
      else {

        db_insert('morris_omniture_var')->fields($data)->execute();

      }

    }
    catch (Exception $e) {
      $transaction->rollback();
      throw $e;
    }

  }

  /**
   * Physically remove vars and dependencies from database.
   * 
   * @param str[] $ids
   *   Collection of variable names.
   * 
   * @return int
   *   Number of deleted items.
   */
  public function purgeById($ids) {

    if (empty($ids)) {
      return;
    }

    /*
     * Get the vars.
     */
    $vars = $this->load($ids);

    /*
     * Wrap everything in a transaction.
     */
    $transaction = db_transaction();

    try {

      /*
       * Unmap the vars.
       */
      foreach ($vars as $var) {
        $var->getPlugin()->unmap();
      }

      /*
       * Delete the vars.
       */
      $query = db_delete('morris_omniture_var');
      $query->condition('id', $ids);
      return $query->execute();

    }
    catch (Exception $e) {
      $transaction->rollback();
      throw $e;
    }

  }

  /**
   * Create base select query.
   * 
   * @param array $conditions
   *   Conditions to apply to base select query.
   * 
   * @return obj
   *   The query object.
   */
  protected function _makeBaseSelectQuery($conditions) {

    $query = db_select('morris_omniture_var', 'v');
    $query->fields('v');

    if (isset($conditions['var'])) {
      foreach ($conditions['var'] as $field => $value) {
        if (strpos($field, '.') === FALSE) {
          $query->condition("v.$field", $value);
        }
        else {
          $query->condition($field, $value);
        }
      }
    }

    return $query;

  }

  /**
   * Map raw result to entity object(s).
   *
   * @param obj $result
   *   The result pointer.
   * 
   * @return MorrisOmniture_Var[]
   *   Collection of MorrisOmniture_Var instances.
   */
  protected function _mapSelectResult($result) {

    /*
     * Map raw data to create collection of var mappings.
     */
    $vars = array();

    /*
     * Map raw data to domain object
     */
    while ($row = $result->fetchAssoc()) {

      if (!isset($vars[$row['id']])) {

        // Create new domain object.
        $var = new MorrisOmniture_Var();

        // Get the plugin.
        $plugin = morris_omniture_var_plugin_load($row['plugin']);

        // Map row to domain object.
        $var->setId($row['id']);
        $var->setPlugin($plugin);
        $var->setGrp($row['grp']);
        $var->setWeight($row['weight']);

      }
      else {
        $var = $vars[$row['id']];
      }

      if (!isset($vars[$row['id']])) {
        $vars[$row['id']] = $var;
      }

    }

    return $vars;

  }

}
