<?php
/**
 * @file
 * The user hook implementations and related functions.
 */

/**
 * Implements hook_permission().
 */
function morris_omniture_permission() {

  $perms = array();

  /*
   * Change configuration settings.
   */
  $perms[MORRIS_OMNITURE_PERM_CONF] = array(
    'title' => t('Administer Morris Omniture configuration'),
    'description' => t('Administer morris omniture configuration. This permission alone does not allow users to create and delete mappings. It only provides access to the main configuration and specification of variable mapping settings.'),
    'warning' => t('You do not have permission to administer morris omniture configuration.'),
  );

  /*
   * This permission allows users to add and remove variable
   * mappings. In most cases this should probably be locked
   * down for people who may not really know what they are
   * doing. It tends to fit more on the structure side of
   * configuration. Thus,adding/removing mappings can have
   * business level implications for those who do not
   * actually understand the proper structure set-forth
   * by a property/business owner.
   */
  $perms[MORRIS_OMNITURE_PERM_VARM] = array(
    'title' => t('Administer Morris Omniture variable mappings'),
    'description' => t('Administer morris omniture variable mappings. Coupled with @perm permission this permission allows users to create and delete variable mappings. This should only be granted to users who understand the business implications of changing variable mappings.', array('@perm' => MORRIS_OMNITURE_PERM_CONF)),
    'warning' => t('You do not have permission to administer morris omniture variable mappings.'),
  );

  /*
   * Manage page mappings.
   */
  $perms[MORRIS_OMNITURE_PERM_OVR] = array(
    'title' => t('Administer Morris Omniture mappings'),
    'description' => t('Administer morris omniture page mappings.'),
    'warning' => t('You do not have permission to administer morris omniture page mappings.'),
  );

  return $perms;

}
