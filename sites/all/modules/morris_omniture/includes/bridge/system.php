<?php
/**
 * @file
 * The system hook implementations and related functions.
 */

/**
 * Implements hook_theme().
 */
function morris_omniture_theme() {
  return array(
    'morris_omniture_mappings_form_var_table' => array(
      'render element' => 'elements',
    ),
  );
}

/**
 * Implements hook_element_info().
 */
function morris_omniture_element_info() {
  return array(
    'morris_omniture_mappings_form_var_table' => array(
      '#theme' => 'morris_omniture_mappings_form_var_table',
    ),
  );
}

/**
 * Implements hook_page_build().
 */
function morris_omniture_page_build(&$page) {

  // Apply the omniture tagging.
  if (!morris_omniture_disable_tracking()) {
    morris_omniture_apply_tracking();
  }

  // Apply advanced debug info.
  if (morris_omniture_debug_mode()) {
    morris_omniture_apply_debug_info();
  }

}
