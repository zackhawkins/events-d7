<?php
/**
 * @file
 * Entity module hook integrations.
 */

/**
 * Implements hook_entity_info().
 */
function morris_omniture_entity_info() {

  $info = array();

  // The mapping (request).
  $info['morris_omniture_mapping'] = array(
    'label' => 'Morris Omniture Mapping',
    'controller class' => 'MorrisOmniture_Mapping_Repository',
    'base table' => 'morris_omniture_mapping',
    'load hook' => 'morris_omniture_mapping_load',
    'uri callback' => 'morris_omniture_mapping_uri',
    'fieldable' => FALSE,
    'entity keys' => array(
      'id' => 'id',
      'label' => 'id',
    ),
  );

  // The variable mapping (property).
  $info['morris_omniture_var'] = array(
    'label' => 'Morris Omniture Variable Mapping',
    'controller class' => 'MorrisOmniture_Var_Repository',
    'base table' => 'morris_omniture_var',
    'load hook' => 'morris_omniture_var_load',
    'uri callback' => 'morris_omniture_var_uri',
    'fieldable' => FALSE,
    'entity keys' => array(
      'id' => 'id',
      'label' => 'id',
    ),
  );

  return $info;

}
