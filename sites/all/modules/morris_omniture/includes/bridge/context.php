<?php
/**
 * @file
 * The context module hook implementations and related functions.
 */

/**
 * Implements hook_context_plugins().
 */
function morris_omniture_context_plugins() {
  $plugins = array();
  $plugins['morris_omniture_context_reaction_mapping'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', MORRIS_OMNITURE_MOD) . '/plugins/context',
      'file' => 'morris_omniture_context_reaction_mapping.inc',
      'class' => 'morris_omniture_context_reaction_mapping',
      'parent' => 'context_reaction',
    ),
  );
  return $plugins;
}

/**
 * Implements hook_context_registry().
 */
function morris_omniture_context_registry() {
  return array(
    'reactions' => array(
      'morris_omniture_mapping' => array(
        'title' => t('Omniture Sitecatalyst'),
        'description' => 'Omniture sitecatalyst mapping overrides.',
        'plugin' => 'morris_omniture_context_reaction_mapping',
      ),
    ),
  );
}

/**
 * Implements hook_context_node_condition().
 *
 * Piggy back on top of context to set information for any
 * plugins that need to be loaded with node related data
 * for node based requests.Modules that declare new node
 * based omniture variable plugins should implement this
 * context hook and follow the same logic.
 */
function morris_omniture_context_node_condition_alter($node, $op) {

  // Get the node plugins.
  $plugin_node_id = morris_omniture_var_plugin_load('node_id');
  $plugin_node_bundle = morris_omniture_var_plugin_load('node_bundle');

  // Associate the node ID.
  if ($node) {
    if (isset($node->nid)) {
      $plugin_node_id->set_node_id($node->nid);
    }

    // Associate the node bundle.
    if (isset($node->type)) {
      $plugin_node_bundle->set_node_bundle($node->type);
    }
  }

}
