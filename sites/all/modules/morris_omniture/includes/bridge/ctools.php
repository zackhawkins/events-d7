<?php
/**
 * @file
 * The ctools hook implementations and related functions/helpers.
 */

/**
 * Implements hook_ctools_plugin_directory().
 */
function morris_omniture_ctools_plugin_directory($module, $plugin) {

  if ($module == 'ctools' && $plugin == 'export_ui') {
    return 'plugins/export_ui';

  }
  elseif ($module == MORRIS_OMNITURE_MOD && $plugin == 'var') {
    return 'plugins/var';
  }

}

/**
 * Implements hook_ctools_plugin_type().
 *
 * Plugins exposed by module.
 * Default settings for morris omniture 
 * variable plugin.
 */
function morris_omniture_ctools_plugin_type() {

  $plugins = array();

  $plugins['var'] = array(
    'process' => 'morris_omniture_process_var',
    'defaults' => array(
      'title' => 'undefined',
      'group' => NULL,
      'overridable' => FALSE,
      'class' => '',
      'description' => NULL,
    ),
  );

  return $plugins;

}

/**
 * The process plugin callback.
 *
 * @param array &$plugin
 *   The plugin data.
 * 
 * @param array $info
 *   The plugin info.
 */
function morris_omniture_process_var(&$plugin, $info) {

  // Get the class name.
  $class = $plugin['class'];

  // Instantiate morris omniture plugin.
  $plugin = new $class($plugin);

}

/**
 * Implements hook_ctools_plugin_post_alter().
 * 
 * Modify morris_omniture export_ui mapping plugins so that
 * when add and edit form is submited redirect occurs back to
 * custom datagrid rather than the default context list.
 */ 
function morris_omniture_ctools_plugin_post_alter(&$plugin,$info) {
    
  if($info['module'] == 'ctools' && $info['type'] == 'export_ui') {
    switch($plugin['name']) {
      case 'morris_omniture_path_mapping':
        $plugin['redirect']['add'] = 'admin/structure/morris-omniture/mappings/paths';
        $plugin['redirect']['edit'] = 'admin/structure/morris-omniture/mappings/paths';
        break;
                
      case 'morris_omniture_term_mapping':
        $plugin['redirect']['add'] = 'admin/structure/morris-omniture/mappings/terms';
        $plugin['redirect']['edit'] = 'admin/structure/morris-omniture/mappings/terms';
        break;
                
      default:
    }
  }
    
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function morris_omniture_form_ctools_export_ui_edit_item_form_alter(&$form, &$form_state) {

  /*
   * Ctools export ui form generated from internal callback
   * for creating term or path mappings.
   */
  if (isset($form_state['function args'][4]) && strpos($form_state['function args'][4], 'morris_omniture_') === 0) {

    // This is how we can separate term and path mappings for building lists.
    $mapping_type = !empty($form_state['item']->tag) ? $form_state['item']->tag : $form_state['function args'][4];

    /*
     * Flag form as being used within the context of
     * creating a context for a morris omniture mapping.
     */
    $form['#attributes']['class'][] = 'morris-omniture-mapping';
    $form['#attributes']['class'][] = 'morris-omniture-mapping-' . str_replace(array('morris_omniture_', '_mapping', '_'), array('', '', '-'), $mapping_type);

    // The title of page.
    if (!empty($form_state['item']->name)) {
      drupal_set_title('Edit Mapping');
    }
    else {
      drupal_set_title('Add Mapping');
    }

    // A little dirty but gets the job done.
    $form['info']['#attributes']['style'] = 'display: none;';

    $form['info']['name']['#access'] = FALSE;
    $form['info']['name']['#type'] = 'value';

    /*
     * When dealing with an existing mapping use that
     * name. Otherwise, generate a new unique name for
     * the mapping.
     */
    $form['info']['name']['#value'] = !empty($form_state['item']->name) ? $form_state['item']->name : morris_omniture_generate_unique_mapping_name();

    /*
     * IMPORTANT
     * Tagging has no other reponsibility besides
     * being able to build separate lists of path
     * and term mappings. NEVER use this to determine whether
     * a context has a path or term mapping because the
     * standard context form can still be used where the tag
     * is a user supplied/controlled value/input.
     */
    $form['info']['tag']['#access'] = FALSE;
    $form['info']['tag']['#type'] = 'value';
    $form['info']['tag']['#value'] = $mapping_type;

    $form['info']['description']['#access'] = FALSE;
    $form['condition_mode']['#access'] = FALSE;

    /*
     * When creating a new mapping require all conditions.
     * Otherwise use what has been specified. This is
     * critial when the domain context module is enabled so
     * that the context is only applied to
     * the specified domains.
     */
    $form['condition_mode']['#value'] = !empty($form_state['item']->name) ? $form_state['item']->condition_mode : 1;

    // $form['conditions']['#title_display'] = 'invisible';
    $form['conditions']['selector']['#access'] = FALSE;

    // $form['reactions']['#title_display'] = 'invisible';
    $form['reactions']['selector']['#access'] = FALSE;
    $form['reactions']['selector']['#default_value'] = 'morris_omniture_mapping';

    /*
     * Change plugin based on type of mapping.
     */
    if (strpos($mapping_type, 'term') !== FALSE) {

      $form['conditions']['plugins']['node_taxonomy']['#context_enabled'] = TRUE;
      $form['conditions']['selector']['#default_value'] = 'node_taxonomy';

      // Require the term.
      $form['conditions']['plugins']['node_taxonomy']['values']['#required'] = TRUE;

    }
    elseif (strpos($mapping_type, 'path') !== FALSE) {

      $form['conditions']['plugins']['path']['#context_enabled'] = TRUE;
      $form['conditions']['selector']['#default_value'] = 'path';

      // Require the path.
      $form['conditions']['plugins']['path']['values']['#required'] = TRUE;

    }

    // Always enable morris omniture reaction.
    $form['reactions']['plugins']['morris_omniture_mapping']['#context_enabled'] = TRUE;

    // Change path to a textfield.
    $form['conditions']['plugins']['path']['values']['#type'] = 'textfield';

    // Hide node taxonomy option for page.
    $form['conditions']['plugins']['node_taxonomy']['options']['node_form']['#access'] = FALSE;

    /*
     * Hide delete button. Mappings should ONLY be deleted
     * via the main listing data grid
     * using the provided operations. This ensures
     * that yahoo apt tables that rely on
     * context are removed along with context.
     * Deleting a context any other way will result
     * in the context being removed without the associated
     * tables in yahoo apt module. Which
     * will not result in an error at this time because foreign
     * keys are no enforced by Drupal
     * but will leave orphan data laying around which is no
     * good and/or if foreign keys are supported
     * in some way in the future would result in a foreigh
     * key constraint violation error.
     */
    $form['buttons']['delete']['#access'] = FALSE;

  }

  $form['#submit'][] = 'morris_omniture_ctools_export_ui_edit_item_form_submit';

}

/**
 * Custom integration with ctools form submit handler.
 * 
 * Work around to get around the fact that there is no way to reference
 * a context on the context ui form from the reaction plugin options
 * submit handler. Instead of persiting the mapping in the plugin options submit
 * handler it will be done here. That way we have easy access to the context
 * item required for the foreign key.
 * 
 * @param array &$form
 *   The form build array.
 * 
 * @param array &$form_state
 *   The form state.
 */
function morris_omniture_ctools_export_ui_edit_item_form_submit(&$form, &$form_state) {

  $mapping = morris_omniture_mapping_form_persist_mapping();

  /*
   * Considering the export ui from can be used in many different
   * conexts checking for the mapping is a surefire way to make
   * certain that the export ui form is being used within the context
   * of the context edit form. A mapping will not exist unless
   * the context edit form has been submitted.
   */
  if ($mapping) {

    // Assign context reference.
    $mapping->setContext((object) array(
      'name' => $form_state['item']->name,
    ));

    // Save mapping to db.
    entity_get_controller('morris_omniture_mapping')->persist($mapping);

  }

}

/**
 * Save mapping to internal static variable to be persisted at a later time. 
 * 
 * This function is called within the context reaction plugin
 * morris_omniture_mapping inside the options_submit method. The mapping
 * is saved here so that it can than be retreived from
 * morris_omniture_ctools_export_ui_edit_item_form_submit
 * where it is actually persisted to the db with the proper context binding.
 *
 * @param MorrisOmniture_Mapping $mapping
 *   The mapping object.
 * 
 * @return MorrisOmniture_mapping
 *   The mapping object.
 */
function morris_omniture_mapping_form_persist_mapping($mapping = NULL) {
  static $cache = NULL;

  if ($mapping) {
    $cache = $mapping;
  }
  else {
    return $cache;
  }
}
