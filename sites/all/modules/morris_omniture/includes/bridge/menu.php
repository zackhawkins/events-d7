<?php
/**
 * @file
 * Menu module hook integrations.
 */

/**
 * Implements hook_menu().
 */
function morris_omniture_menu() {

  $items = array();

  // Config: general settings.
  $items['admin/config/system/morris-omniture'] = array(
    'title' => 'Adobe SiteCatalyst',
    'description' => 'Define default settings for adobe SiteCatalyst and variable mappings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('morris_omniture_config_form'),
    'access callback' => 'morris_omniture_access_config',
    'file path' => drupal_get_path('module', MORRIS_OMNITURE_MOD) . '/includes/form',
    'file' => 'config.php',
  );

  // Config: general settings.
  $items['admin/config/system/morris-omniture/general'] = array(
    'title' => 'General',
    'description' => 'Omniture',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('morris_omniture_config_form'),
    'access callback' => 'morris_omniture_access_config',
    'file path' => drupal_get_path('module', MORRIS_OMNITURE_MOD) . '/includes/form',
    'file' => 'config.php',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  // Config: variable mappings interactive data grid.
  $items['admin/config/system/morris-omniture/mappings'] = array(
    'title' => 'Variables',
    'description' => 'Omniture variable mappings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('morris_omniture_mappings_form'),
    'access callback' => 'morris_omniture_access_config',
    'file path' => drupal_get_path('module', MORRIS_OMNITURE_MOD) . '/includes/form',
    'file' => 'mappings.php',
    'type' => MENU_LOCAL_TASK,
  );

  /*
   * -------------------------------------------------
   */

  // Main structure link.
  $items['admin/structure/morris-omniture'] = array(
    'title' => 'Adobe Sitecatalyst',
    'description' => 'Manage SiteCatalyst mapping overrides.',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array(MORRIS_OMNITURE_PERM_OVR),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );

  // Mappings link.
  $items['admin/structure/morris-omniture/mappings'] = array(
    'title' => 'Mappings',
    'description' => 'Administer page mappings for overriding SiteCatalyst configuration.',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array(MORRIS_OMNITURE_PERM_OVR),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );

  // The path mappings.
  $items['admin/structure/morris-omniture/mappings/paths'] = array(
    'title' => 'Path Mappings',
    'description' => 'Administer path mappings for overriding SiteCatalyst configuration.',
    'page callback' => 'morris_omniture_mapping_datagrid_page',
    'page arguments' => array(4),
    'access arguments' => array(MORRIS_OMNITURE_PERM_OVR),
    'file path' => drupal_get_path('module', MORRIS_OMNITURE_MOD) . '/includes/admin',
    'file' => 'mapping_datagrid.php',
  );

  // The term mappings.
  $items['admin/structure/morris-omniture/mappings/terms'] = array(
    'title' => 'Term Mappings',
    'description' => 'Administer term mappings for overriding SiteCatalyst configuration.',
    'page callback' => 'morris_omniture_mapping_datagrid_page',
    'page arguments' => array(4),
    'access arguments' => array(MORRIS_OMNITURE_PERM_OVR),
    'file path' => drupal_get_path('module', MORRIS_OMNITURE_MOD) . '/includes/admin',
    'file' => 'mapping_datagrid.php',
  );

  /*
   * The whole goal here is to make the transition from
   * CASAA to this module easier. That requires essentially
   * customizing the context form to better represent the forms
   * that existed in casaa for the omniture plugin module. This
   * This is achieved by handing off the request to the standard
   * ctools export ui page handler as if the standard context edit
   * path was being used. The only difference is an extra argument
   * which is used in the form alter hook to determine whether
   * the form is being used within our context. A little tricky
   * but I think well worth the user experience improvement and
   * cutting off people from standard context form which can be
   * very confussing. This essentially hides all the stuff that is
   * not relevant to the daily operations of ad mappings for terms
   * and paths, like casaa did but handled all by context module.
   * The same thing goes for the edit path below.
   *
   * Also mappings do not need to be created through here.
   * They can be created through the standard context form.
   * This is just a cleaned up version only presenting options
   * applicable to omniture mapppings while leaving
   * everything else hidden.
   */
  $items['admin/structure/morris-omniture/mappings/paths/add'] = array(
    'title' => 'Add Path Mapping',
    'description' => 'Create new SiteCatalyst mapping.',
    'page callback' => 'ctools_export_ui_switcher_page',
    'page arguments' => array(
      'morris_omniture_path_mapping',
      'add',
      NULL,
      NULL,
      'morris_omniture_path_mapping',
    ),
    'load arguments' => array('context'),
    'access arguments' => array(MORRIS_OMNITURE_PERM_OVR),
    'file path' => drupal_get_path('module', 'ctools'),
    'file' => 'includes/export-ui.inc',
    'type' => MENU_LOCAL_ACTION,
  );

  $items['admin/structure/morris-omniture/mappings/terms/add'] = array(
    'title' => 'Add Term Mapping',
    'description' => 'Create new SiteCatalyst term mapping.',
    'page callback' => 'ctools_export_ui_switcher_page',
    'page arguments' => array(
      'morris_omniture_term_mapping',
      'add',
      NULL,
      NULL,
      'morris_omniture_term_mapping',
    ),
    'load arguments' => array('context'),
    'access arguments' => array(MORRIS_OMNITURE_PERM_OVR),
    'file path' => drupal_get_path('module', 'ctools'),
    'file' => 'includes/export-ui.inc',
    'type' => MENU_LOCAL_ACTION,
  );

  $items['admin/structure/morris-omniture/mappings/terms/%ctools_export_ui/edit'] = array(
    'title' => 'Edit Term Mapping',
    'description' => 'Edit existing SiteCatalyst term mapping.',
    'page callback' => 'ctools_export_ui_switcher_page',
    'page arguments' => array(
      'morris_omniture_term_mapping',
      'edit',
      5,
      NULL,
      'morris_omniture_term_mapping',
    ),
    'load arguments' => array('context'),
    'access arguments' => array(MORRIS_OMNITURE_PERM_OVR),
  );

  $items['admin/structure/morris-omniture/mappings/paths/%ctools_export_ui/edit'] = array(
    'title' => 'Edit Path Mapping',
    'description' => 'Edit existing SiteCatalyst path mapping.',
    'page callback' => 'ctools_export_ui_switcher_page',
    'page arguments' => array(
      'morris_omniture_path_mapping',
      'edit',
      5,
      NULL,
      'morris_omniture_path_mapping',
    ),
    'load arguments' => array('context'),
    'access arguments' => array(MORRIS_OMNITURE_PERM_OVR),
  );

  /*
   * Dump data in morris omniture variable table
   */
  $items['admin/morris-omniture-var-export/php'] = array(
    'title' => 'Export Morris SiteCatalyst variables',
    'description' => 'Export SiteCatalyst variables',
    'page callback' => 'morris_omniture_var_export_php',
    'access arguments' => array(MORRIS_OMNITURE_PERM_OVR),
    'file path' => drupal_get_path('module', MORRIS_OMNITURE_MOD) . '/includes/admin',
    'file' => 'var_export_php.php',
    'type' => MENU_CALLBACK,
  );

  return $items;

}

/**
 * Whether user is able to access morris omniture configuration pages. 
 * 
 * This is necessary because access to page/operations 
 * is granted when user has one of two permissions. This is done 
 * because one permission controls whether the user can merely 
 * change settings and add/remove variable mappings.
 * 
 * @return bool
 *   Determine whether user has access to configuration area.
 */
function morris_omniture_access_config() {
  return user_access(MORRIS_OMNITURE_PERM_VARM) || user_access(MORRIS_OMNITURE_PERM_CONF);
}
