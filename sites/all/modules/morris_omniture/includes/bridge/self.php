<?php
/**
 * @file
 * The morris_omniture hook implementations.
 */

/**
 * Implements hook_morris_omniture_mapping_load_query_alter().
 */
function morris_omniture_morris_omniture_mapping_load_query_alter($query, $ids, $conditions) {
}

/**
 * Implements hook_morris_omniture_mapping_persist().
 */
function morris_omniture_morris_omniture_mapping_persist($mapping) {

  $data = array(
    'category' => $mapping->get('morris.category'),
    'subcategory' => $mapping->get('morris.subcategory'),
    'page_name' => $mapping->get('morris.page_name'),
    'channel' => $mapping->get('morris.channel'),
    'tracking_disabled'=> ((int) $mapping->get('tracking_disabled')),
  );

  db_update('morris_omniture_mapping')->fields($data)->condition('id', $mapping->getId())->execute();

}

/**
 * Implements hook_morris_omniture_map_select_result().
 */
function morris_omniture_morris_omniture_mapping_map_select_result($mapping, $row) {

  $mapping->set('morris.category', $row['category']);
  $mapping->set('morris.subcategory', $row['subcategory']);
  $mapping->set('morris.page_name', $row['page_name']);
  $mapping->set('morris.channel', $row['channel']);
  
  $mapping->set('tracking_disabled',((bool) ((int) $row['tracking_disabled'])));

}

/**
 * Implements hook_morris_omniture_mapping_purge().
 */
function morris_omniture_morris_omniture_mapping_purge($context) {
}
