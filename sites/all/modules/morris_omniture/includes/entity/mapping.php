<?php
/**
 * @file
 * The domain mapping entity.
 */

class MorrisOmniture_Mapping {

  protected $id;
  protected $context;
  protected $attr = array();

  /**
   * Set the primary key for the mapping.
   * 
   * @param int $id
   *   The mapping primary key.
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * Get the primary key for the mapping.
   * 
   * @return int
   *   The mapping primary key.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set the context associated with the mapping.
   * 
   * @param StdClass $context
   *   The context object.
   */
  public function setContext($context) {
    $this->context = $context;
  }

  /**
   * Get the context associated with the mapping.
   * 
   * @return StdClass
   *   The context object.
   */
  public function getContext() {
    return $this->context;
  }

  /**
   * Overload meta data property.
   *
   * @param str $name
   *   The metadata name.
   * 
   * @param mix $value
   *   The metadata value.
   */
  public function set($name, $value) {
    $this->attr[$name] = $value;
  }

  /**
   * Get overloaded meta data property.
   *
   * @param str $name
   *   The metadata name.
   * 
   * @return mix
   *   The metadata value.
   */
  public function get($name) {
    return isset($this->attr[$name]) ? $this->attr[$name] : NULL;
  }

}
