<?php
/**
 * @file
 * The domain var entity.
 */

class MorrisOmniture_Var {

  protected $id;
  protected $plugin;
  protected $grp;
  protected $weight = 0;

  /**
   * Set the primary key for the variable.
   * 
   * @param int $id
   *   The variable primary key.
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * Get the primary key for the variable.
   * 
   * @return int
   *   The variable primary key.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set the group for the variable.
   * 
   * @param str $grp
   *   The group for the variable.
   */
  public function setGrp($grp) {
    $this->grp = $grp;
  }

  /**
   * Get the group for variable.
   * 
   * @return str
   *   The group for the variable.
   */
  public function getGrp() {
    return $this->grp;
  }

  /**
   * Set the weight for the variable.
   * 
   * @param int $weight
   *   The weight for the variable.
   */
  public function setWeight($weight) {
    $this->weight = $weight;
  }

  /**
   * Get the weight for the variable.
   * 
   * @return int
   *   The weight for the variable.
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * Set the associated plugin.
   *
   * @param morris_omniture_ivar $plugin
   *   The variable plugin instance.
   */
  public function setPlugin(morris_omniture_ivar $plugin) {
    $this->plugin = $plugin;
  }

  /**
   * Get the associated plugin.
   *
   * @return morris_omniture_ivar
   *   The variable instance.
   */
  public function getPlugin() {
    return $this->plugin;
  }

}
