<?php
/**
 * @file
 * Context reaction plugin for variable mappings.
 */

class morris_omniture_context_reaction_mapping extends context_reaction {

  /**
   * Context reaction options form.
   * 
   * @param StdClass $context
   *   The context object.
   * 
   * @return array
   *   The mapping options.
   */
  public function options_form($context) {

    $form = array();

    $values = $this->fetch_from_context($context);

    $mapping = NULL;
    if ($context->name) {
      $mapping = morris_omniture_mapping_load_by_context($context->name);
    }

    if (!$mapping) {
      $mapping = new MorrisOmniture_Mapping();
    }

    $form['mapping'] = array(
      '#type' => 'fieldset',
      '#title' => 'SiteCatalyst Settings',
      '#description' => 'Unspecified settings will fallback to global defaults.',
    );
    
    // Disable tracking
    $form['mapping']['tracking_disabled'] = array(
      '#type' => 'checkbox',
      '#title' => 'Disable Tracking',
      '#description' => 'Disable Site Catalyst tagging.',
      '#default_value' => (int) $mapping->get('tracking_disabled'),
    );

    // The channel override.
    $form['mapping']['channel'] = array(
      '#type' => 'textfield',
      '#title' => 'Channel',
      '#description' => 'Channel override.',
      '#default_value' => $mapping->get('morris.channel'),
    );

    // Expose form for every omniture plugin with overrideable options.
    foreach (morris_omniture_get_props() as $prop) {
      $plugin = $prop->getPlugin();
      if ($plugin->overridable()) {
        $form['mapping'][$plugin->get_name()] = $plugin->override_form($context);
      }
    }

    // This seems like the simplest method to persist the mapping id.
    if ($mapping->getId()) {
      $form['mapping']['id'] = array(
        '#type' => 'value',
        '#value' => $mapping->getId(),
      );
    }

    return $form;

  }

  /**
   * The options form submit handler.
   * 
   * @param array $values
   *   The submitted form values.
   * 
   * @return array
   *   Data to add to context for reaction.
   */
  public function options_form_submit($values) {

    // Create the mapping.
    $mapping = new MorrisOmniture_Mapping();

    // When editing existing mapping set ID to trigger update.
    if (isset($values['mapping']['id']) && !empty($values['mapping']['id'])) {
      $mapping->setId($values['mapping']['id']);
    }

    /*
     * The mapping is saved using a static variable. Later a submit
     * hook is used so that the actual context can be referenced.
     * I don't really see any better way to get at the context name...
     */
    morris_omniture_mapping_form_persist_mapping($mapping);

    // Set the disable tracking variable.
    if (!empty($values['mapping']['tracking_disabled'])) {
        $mapping->set('tracking_disabled',((bool) ((int) $values['mapping']['tracking_disabled'])));
    }

    // Set the channel override.
    if (!empty($values['mapping']['channel'])) {
      $mapping->set('morris.channel', $values['mapping']['channel']);
    }

    // Hand off to other functions.
    foreach (morris_omniture_get_props() as $prop) {
      $plugin = $prop->getPlugin();
      if ($plugin->overridable()) {
        $form['mapping'][$plugin->get_name()] = $plugin->override_submit($values['mapping'][$plugin->get_name()]);
      }
    }

    return array('mapping' => TRUE);
  }

}
