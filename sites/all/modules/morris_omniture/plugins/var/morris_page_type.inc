<?php
/**
 * @file
 * The special morris page type.
 */

$plugin = array(
  'class' => 'morris_omniture_var_morris_page_type',
  'title' => 'Page Type',
  'group' => 'Morris',
  'overridable' => FALSE,
);

class morris_omniture_var_morris_page_type extends morris_omniture_var_base {

  const VAR_NAME = 'morris_omniture_var_page_type';

  /**
   * Main settings form for variable mapping.
   * 
   * @param array &$form_state
   *   The settings form state data.
   * 
   * @return array
   *   Form partial with variable specific fields.
   */
  public function settings(&$form_state) {

    $form = array();

    $form['page_type'] = array(
      '#title' => 'Page Type',
      '#type' => 'textfield',
      '#default_value' => variable_get(self::VAR_NAME, NULL),
      '#field_suffix' => ' + location.pathname',
    );

    return $form;

  }

  /**
   * The settings form submit handler.
   * 
   * @param array $values
   *   The submitted form values.
   */
  public function settings_submit($values) {
    variable_set(self::VAR_NAME, $values['page_type']);
  }

  /**
   * Remove global page type.
   */
  public function unmap() {

    // Remove the global.
    variable_del(self::VAR_NAME);

  }

  /**
   * Get end output value.
   * 
   * @return mix
   *   The end output value.
   */
  public function val() {

    $config = morris_omniture_default_config();
    $override = variable_get(self::VAR_NAME, NULL);

    return morris_omniture_js_snippet_wrapper('"' . ($override ? $override : $config['server']) . '" + location.pathname;');

  }

}
