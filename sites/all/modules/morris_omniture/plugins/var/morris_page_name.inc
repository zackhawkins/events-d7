<?php
/**
 * @file
 * The special morris page name variable.
 */

$plugin = array(
  'class' => 'morris_omniture_var_morris_page_name',
  'title' => 'Page Name',
  'group' => 'Morris',
  'overridable' => TRUE,
);

class morris_omniture_var_morris_page_name extends morris_omniture_var_base {

  /**
   * Form partial to override the page name.
   * 
   * @param StdClass $context
   *   The associated context object.
   * 
   * @return array
   *   The form build array.
   */
  public function override_form($context) {

    $mapping = $this->fetch_mapping_from_context($context);

    $form = array();

    $form['page_name'] = array(
      '#title' => 'Page Name',
      '#type' => 'textfield',
      '#default_value' => $mapping->get('morris.page_name'),
      '#description' => 'Page name override.',
    );

    return $form;

  }

  /**
   * The submit handler for override form.
   * 
   * @param array $values
   *   The submitted values.
   */
  public function override_submit($values) {

    $mapping = $this->fetch_request_mapping();

    if (!empty($values['page_name'])) {
      $mapping->set('morris.page_name', $values['page_name']);
    }

  }

  /**
   * Remove all page name overrides.
   */
  public function unmap() {

    // Set mapping page_name column to null.
    db_update('morris_omniture_mapping')->fields(array('page_name' => NULL))->execute();

  }

  /**
   * Get end output value for current request.
   * 
   * @return mix
   *   The end output value.
   */
  public function val() {

    $default = morris_omniture_js_snippet_wrapper('document.title;');

    $mapping = morris_omniture_discover_mapping();

    return $mapping && $mapping->get('morris.page_name') ? $mapping->get('morris.page_name') : $default;

  }

}
