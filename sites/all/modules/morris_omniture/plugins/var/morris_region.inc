<?php
/**
 * @file
 * Special morris region variable. 
 */

$plugin = array(
  'class' => 'morris_omniture_var_morris_region',
  'title' => 'Region',
  'group' => 'Morris',
  'overridable' => FALSE,
);

class morris_omniture_var_morris_region extends morris_omniture_var_base {

  const VAR_NAME = 'morris_omniture_var_morris_region';

  /**
   * Region settings form.
   * 
   * @param array $form_state
   *   The region form state.
   * 
   * @return array
   *   Additional fields for region mapping.
   */
  public function settings(&$form_state) {

    $form = array();

    $form['region'] = array(
      '#title' => 'region',
      '#type' => 'select',
      '#options' => _morris_omniture_region_options(),
      '#default_value' => variable_get(self::VAR_NAME, NULL),
    );

    return $form;

  }

  /**
   * Region settings form submit handler.
   * 
   * @param array $values
   *   The submitted values.
   */
  public function settings_submit($values) {
    variable_set(self::VAR_NAME, $values['region']);
  }

  /**
   * Delete region variable.
   */
  public function unmap() {
    variable_del(self::VAR_NAME);
  }

  /**
   * The end output variable.
   * 
   * @return mix
   *   The value sent to omniture Site Catalyst.
   */
  public function val() {

    $default = variable_get(self::VAR_NAME, '');

    return $default;

  }

}
