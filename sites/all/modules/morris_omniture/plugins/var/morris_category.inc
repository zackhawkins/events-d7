<?php
/**
 * @file
 * Morris communications category.
 */

$plugin = array(
  'class' => 'morris_omniture_var_morris_category',
  'title' => 'Category',
  'group' => 'Morris',
  'overridable' => TRUE,
);

class morris_omniture_var_morris_category extends morris_omniture_var_base {

  const VAR_NAME = 'morris_omniture_var_morris_category';

  /**
   * The plugin settings.
   * 
   * @param array &$form_state
   *   The form state.
   * 
   * @return array
   *   The form partial.
   */
  public function settings(&$form_state) {

    $form = array();

    $form['category'] = array(
      '#title' => 'Category',
      '#type' => 'select',
      '#options' => _morris_omniture_category_options(),
      '#default_value' => variable_get(self::VAR_NAME, NULL),
    );

    return $form;

  }

  /**
   * Settings submit handler.
   * 
   * @param array $values
   *   The submitted values.
   */
  public function settings_submit($values) {
    variable_set(self::VAR_NAME, $values['category']);
  }

  /**
   * The override form.
   * 
   * @param StdClass $context
   *   The context form context object.
   * 
   * @return array
   *   The partial form.
   */
  public function override_form($context) {

    $mapping = $this->fetch_mapping_from_context($context);

    $form_state = array();
    $form = $this->settings($form_state);
    $form['category']['#default_value'] = $mapping->get('morris.category');

    return $form;

  }

  /**
   * The override form submit handler.
   * 
   * @param array $values
   *   The submitted values.
   */
  public function override_submit($values) {

    $mapping = $this->fetch_request_mapping();

    if (!empty($values['category'])) {
      $mapping->set('morris.category', $values['category']);
    }

  }

  /**
   * Action occurs when var mapping is deleted.
   */
  public function unmap() {

    // Delete the global.
    variable_del(self::VAR_NAME);

    // Set mapping category column to null.
    db_update('morris_omniture_mapping')->fields(array('category' => NULL))->execute();
  }

  /**
   * The end output value.
   * 
   * @return str
   *   The value sent to omniture.
   */
  public function val() {

    $default = variable_get(self::VAR_NAME, '');
    $mapping = morris_omniture_discover_mapping();

    return $mapping && $mapping->get('morris.category') ? $mapping->get('morris.category') : $default;

  }

}
