<?php
/**
 * @file
 * Morris category variable.
 */

$plugin = array(
  'class' => 'morris_omniture_var_morris_subcategory',
  'title' => 'Sub Category',
  'group' => 'Morris',
  'overridable' => TRUE,
);

class morris_omniture_var_morris_subcategory extends morris_omniture_var_base {

  const VAR_NAME = 'morris_omniture_var_morris_subcategory';

  /**
   * The subcategory settings form.
   * 
   * @param array &$form_state
   *   The form state.
   * 
   * @return array
   *   The additional settings fields for mapping.
   */
  public function settings(&$form_state) {

    $form = array();

    $form['subcategory'] = array(
      '#title' => 'Sub Category',
      '#type' => 'select',
      '#options' => _morris_omniture_subcategory_options(),
      '#default_value' => variable_get(self::VAR_NAME, NULL),
    );

    return $form;

  }

  /**
   * The settings form submit handler.
   * 
   * @param array $values
   *   The submitted form values.
   */
  public function settings_submit($values) {
    variable_set(self::VAR_NAME, $values['subcategory']);
  }

  /**
   * Override form build array.
   * 
   * @param StdClass $context
   *   The associated context.
   * 
   * @return array
   *   The context form reaction form partial.
   */
  public function override_form($context) {

    $mapping = $this->fetch_mapping_from_context($context);

    $form_state = array();
    $form = $this->settings($form_state);
    $form['subcategory']['#default_value'] = $mapping->get('morris.subcategory');

    return $form;

  }

  /**
   * The context form reaction submit handler.
   * 
   * @param array $values
   *   The submitted values.
   */
  public function override_submit($values) {

    $mapping = $this->fetch_request_mapping();

    if (!empty($values['subcategory'])) {
      $mapping->set('morris.subcategory', $values['subcategory']);
    }

  }

  /**
   * Remove global default and override values for variable mapping.
   */
  public function unmap() {

    // Remove the global.
    variable_del(self::VAR_NAME);

    // Set mapping subcategory column to null.
    db_update('morris_omniture_mapping')->fields(array('subcategory' => NULL))->execute();

  }

  /**
   * The end output value.
   * 
   * @return str
   *   The value sent to Site Catalyst.
   */
  public function val() {

    $default = variable_get(self::VAR_NAME, '');
    $mapping = morris_omniture_discover_mapping();

    return $mapping && $mapping->get('morris.subcategory') ? $mapping->get('morris.subcategory') : $default;

  }

}
