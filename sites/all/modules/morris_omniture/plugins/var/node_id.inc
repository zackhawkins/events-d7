<?php
/**
 * @file
 * For a node page the node ID.
 */

$plugin = array(
  'class' => 'morris_omniture_var_node_id',
  'title' => 'ID',
  'group' => 'Node',
  'overridable' => FALSE,
);

class morris_omniture_var_node_id extends morris_omniture_var_base {

  protected $node_id;

  /**
   * Get output value.
   *
   * @return str
   *   The node ID.
   */
  public function val() {
    if ($this->node_id) {
      return (string) $this->node_id;
    }
  }

  /**
   * Set node ID.
   *
   * @param int $id
   *   The node ID.
   */
  public function set_node_id($id) {
    $this->node_id = $id;
  }

}
