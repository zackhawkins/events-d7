<?php
/**
 * @file
 * Node page node bundle (content type).
 */

$plugin = array(
  'class' => 'morris_omniture_var_node_bundle',
  'title' => 'Content Type',
  'group' => 'Node',
  'overridable' => FALSE,
);

class morris_omniture_var_node_bundle extends morris_omniture_var_base {

  protected $node_bundle;

  /**
   * Get output value.
   *
   * @return str
   *   The node bundle.
   */
  public function val() {
    if ($this->node_bundle) {
      return (string) $this->node_bundle;
    }
  }

  /**
   * Set node bundle.
   *
   * @param str $bundle
   *   The node bundle.
   */
  public function set_node_bundle($bundle) {
    $this->node_bundle = $bundle;
  }

}
