<?php
/**
 * @file
 * JavaScript document.title plugin.
 */

$plugin = array(
  'class' => 'morris_omniture_var_doc_title',
  'title' => 'document.title',
  'group' => 'JavaScript',
  'overridable' => FALSE,
);

class morris_omniture_var_doc_title extends morris_omniture_var_base {

  /**
   * Get plugin value.
   * 
   * @return StdClass
   *   JavaScript code snippet wrapper.
   */
  public function val() {

    return morris_omniture_js_snippet_wrapper('document.title;');

  }

}
