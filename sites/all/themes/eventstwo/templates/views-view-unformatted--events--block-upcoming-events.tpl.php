<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div id="upcoming_event_content">
	<div id="event_content_sorting_type">
		<ul id="sorting_type_list">
		<li class="small_content_item header_when">When</li>
		<li class="small_content_item header_what">What</li>
		<li class="small_content_item header_where">Where</li>
		<li class="small_content_item header_how">How</li>
	  </ul>
	</div>
	<hr class="large_red_divider" />

	<div id="event_content_area_display">

		<?php foreach ($rows as $id => $row): ?>
		  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
			<?php print $row; ?>
		  </div>
		<?php endforeach; ?>

	</div>
</div>