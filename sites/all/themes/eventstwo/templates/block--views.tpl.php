<?php
// kpr($variables);
// kpr($variables['template_files']);
//  http://api.drupal.org/api/drupal/modules--block--block.tpl.php/7
if ($classes) {
  $classes = ' class="'. $classes . ' "';
}
?>

<?php if( theme_get_setting('mothership_poorthemers_helper') ){ ?>
<!-- block -->
<?php } ?>
<div <?php print $id_block . $classes .  $attributes; ?>>
  <?php print $mothership_poorthemers_helper;  ?>

  <?php if ($block->subject): ?>
	<div id="upcoming_title" class="large_title_background">
		<h2<?php print $title_attributes; ?>><?php print $block->subject ?></h2>
	</div>
  <?php endif;?>

  <?php if (!theme_get_setting('mothership_classes_block_contentdiv') AND $block->module == "block"): ?>
  <div <?php print $content_attributes; ?>>
  <?php endif ?>

  <?php print $content ?>

  <?php if (!theme_get_setting('mothership_classes_block_contentdiv') AND $block->module == "block"): ?>
  </div>
  <?php endif ?>
</div>
<?php if( theme_get_setting('mothership_poorthemers_helper') ){ ?>
<!-- /block -->
<?php } ?>