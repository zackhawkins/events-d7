<?php
//kpr(get_defined_vars());
//kpr($theme_hook_suggestions);
//template naming
//page--[CONTENT TYPE].tpl.php
?>
<?php if( theme_get_setting('mothership_poorthemers_helper') ){ ?>
<!-- page.tpl.php -->
<?php } ?>

<?php print $mothership_poorthemers_helper; ?>

<div id="page_container">

	<header role="banner" id="page_header">
	  <div class="siteinfo">
		<?php if ($logo): ?>
		  <figure id="header_masthead">
		  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="masthead_logo">
			<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
		  </a>
		  </figure>
		<?php endif; ?>
			<a href="#menu" class="menu-link">Menu</a>
	  </div>

	  <?php if($page['header']): ?>
		<div class="header-region">
		  <?php print render($page['header']); ?>
		</div>
	  <?php endif; ?>

	  <hr class="solid_divider" />

	</header>

	<?php if($page['featured']): ?>
	<div class="featured">
		<?php print render($page['featured']); ?>
	</div>
	<?php endif; ?>

    <hr class="large_teal_divider" />
	<div class="page">

	  <div role="main" id="main-content">

		<?php //print $breadcrumb; ?>

		<?php if ($action_links): ?>
		  <ul class="action-links"><?php print render($action_links); ?></ul>
		<?php endif; ?>

		<?php if (isset($tabs['#primary'][0]) || isset($tabs['#secondary'][0])): ?>
		  <nav class="tabs"><?php print render($tabs); ?></nav>
		<?php endif; ?>

		<?php if($page['highlighted'] OR $messages){ ?>
		  <div class="drupal-messages">
		  <?php print render($page['highlighted']); ?>
		  <?php print $messages; ?>
		  </div>
		<?php } ?>


		<?php print render($page['content_pre']); ?>

		<?php

            print render($page['content']);
            //print_r($page['content']);

		?>

		<?php print render($page['content_post']); ?>

	  </div><!-- /main -->

	  <?php if ($page['sidebar_first']): ?>
		<div class="sidebar-first">
		<?php print render($page['sidebar_first']); ?>
		</div>
	  <?php endif; ?>

	  <?php if ($page['sidebar_second']): ?>
		<div class="sidebar-second">
		  <?php print render($page['sidebar_second']); ?>
		</div>
	  <?php endif; ?>
	</div><!-- /page -->

	<footer role="contentinfo" id="page_footer">
	  <?php print render($page['footer']); ?>
	</footer>

</div><!--/page_container-->

