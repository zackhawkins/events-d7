/**
 * Populates Widget of Upcoming Events
 */
var EventsWidget = (function () {
    var jQuery; // Localize jQuery variable
    var st;

    return {

        //general settings for our module
        settings: {

          baseurl: 'http://mpgdev.dev3.webenabled.net', //no trailing slash
          numitems: 5

        },

        init: function() {

          st = this.settings;
          ewgt = this;

          //load jQuery if not present
          if (window.jQuery === undefined) {
              var script_tag = document.createElement('script');
              script_tag.setAttribute("type","text/javascript");
              script_tag.setAttribute("src",
                  "http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js");
              if (script_tag.readyState) {
                script_tag.onreadystatechange = function () { // For old versions of IE
                    if (this.readyState == 'complete' || this.readyState == 'loaded') {
                        this.scriptLoadHandler();
                    }
                };
              } else {
                script_tag.onload = this.scriptLoadHandler;
              }
              // Try to find the head, otherwise default to the documentElement
              (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
          } else {
              // The jQuery version on the window is the one we want to use
              jQuery = window.jQuery;
              ewgt.main();
          }
        },

        scriptLoadHandler: function() {
          // Restore $ and window.jQuery to their previous values and store the
          // new jQuery in our local jQuery variable
          jQuery = window.jQuery.noConflict(true);
          //fire of main content function
          ewgt.main();

        },

        main: function() {

          jQuery(document).ready(function($) {

            //load the css file
            var css_link = $("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: st.baseurl + "/sites/all/themes/eventstwo/widget/events-widget-style.css"
            });
            css_link.appendTo('head');

            //check to see if we need to return more items
            ewgt.getNumitems($);

            //get the json feed
            ewgt.grabFeed($);

          });

        },

        grabFeed: function($){

            var jsonp_url = st.baseurl + '/services/feed/json/widget?jax_events=?';
            $.getJSON(jsonp_url, function(data) {
              var output = '<div id="events-widget">';

              //get the date headings
              output = output + '<ul class="events-days">' + data.events[0].event.header + '</ul>';
              output = output + '<h3><a href="' + st.baseurl + '/events" target="_blank">Upcoming Events<\/a></h3><ul class="events-list">';

              //grab each list item
              $.each( data.events, function( key, value ) {
                output = output + value.event.html;
                return ( parseFloat(key) !== st.numitems );
              });
              output = output + '<\/ul>';
              output = output + '<a href="' + st.baseurl + '/events" target="_blank" class="events-more"><span>See More Events<\/span><\/a><\/div>';

              //output the html
              $('#events-script').after(output);
            });

        },

        getNumitems: function($) {
          //check the data-attribute parameters for different numitems
          var newnumitems = $('#events-script').attr('data-numitems');
          st.numitems = (newnumitems)? newnumitems - 1 : st.numitems - 1; //subtract 1 to account for the zero of the array
        }

    };
})();

EventsWidget.init();