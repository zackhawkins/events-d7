/**
 * Adds menu functionality for smaller screens
 */
var MobileMenu = (function () {
    var st; // private alias to settings

    return {
        settings: {
          $menu: jQuery('#top_navi_list'),
          $menuLink: jQuery('.menu-link'),
          $search: jQuery('.search-box'),
          $searchLink: jQuery('#top_navi_list a[href="/search"]'),
          $searchBox: jQuery('#edit-title')
        },

        init: function() {
            st = this.settings;
            this.bindUIActions();
        },

        bindUIActions: function() {
          st.$menuLink.click(function() {
            st.$menuLink.toggleClass('active');
            st.$menu.toggleClass('active');
            return false;
          });

          st.$searchLink.click(function() {
            st.$searchLink.toggleClass('active');
            st.$search.toggleClass('active');
            st.$searchBox.focus();
            return false;
          });

        }

    };
})();

(function($) {

    $(function() {

      MobileMenu.init();

    });

}(jQuery));