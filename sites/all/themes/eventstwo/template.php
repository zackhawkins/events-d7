<?php
/*
  Preprocess
*/

/*
function eventstwo_preprocess_html(&$vars) {
  //  kpr($vars['content']);
}
*/
function eventstwo_preprocess_page(&$vars,$hook) {

  //googlefont
    drupal_add_css('http://fonts.googleapis.com/css?family=Arvo:400,700,400italic|PT+Sans:400,700,400italic','external');

  //add authenticated user specific css
  if(user_is_logged_in()):
    drupal_add_css(drupal_get_path('theme', 'eventstwo') . '/css/loggedin.css', array('group' => CSS_THEME, 'weight' => 15));
  endif;
}
/*
function eventstwo_preprocess_region(&$vars,$hook) {
  //  kpr($vars['content']);
}
*/

function eventstwo_preprocess_block(&$vars, $hook) {
  //kpr($vars['content']);

  //lets look for unique block in a region $region-$blockcreator-$delta
   $block =
   $vars['elements']['#block']->region .'-'.
   $vars['elements']['#block']->module .'-'.
   $vars['elements']['#block']->delta;

   //print $block .' ';
   switch ($block) {
     case 'header-system-main-menu ':
       $vars['classes_array'][] = '';
       break;
     case 'header-views--exp-events-page_1':
       $vars['classes_array'][] = 'search-box';
       break;
     case 'sidebar-system-navigation':
       $vars['classes_array'][] = '';
       break;
    default:

    break;

   }

  /*
  switch ($vars['elements']['#block']->region) {
    case 'header':
      $vars['classes_array'][] = '';
      break;
    case 'sidebar':
      $vars['classes_array'][] = '';
      $vars['classes_array'][] = '';
      break;
    default:

      break;
  }
   */
}

function eventstwo_preprocess_node(&$vars,$hook) {
  $node = $vars['node'];
  if ($node->type == 'event') {
    //custom heading for events content-type

    // is repeated event?
    if (isset($node->field_date['und'][0]['rrule'])) {
      $repeat = date_repeat_rrule_description($node->field_date['und'][0]['rrule']);
      $repeat = preg_replace("/\d{1,} times/i", '', $repeat);

      //grab all dates associated with a repeat event
      $allthedates = field_view_field('node', $node,'field_date');
      $todaysdate = strtotime('now');

      //make sure we're using the next available date because they're all in one big array #drupal
      foreach($allthedates['#items'] as $that => $fielddate):
        $datestring = strtotime($fielddate['value']);
        if($datestring >= $todaysdate){
          break;
        }
      endforeach;

    } else {

      //not a repeat event so we can grab the first field_date
      $datestring = strtotime($node->field_date['und'][0]['value']);

    }


    $datemonthday = format_date($datestring ,'custom','n/j');
    $dateplainday = format_date($datestring ,'custom','l');
    $datetime = format_date($datestring ,'custom','h:i A');

    $newdate = '<div class="events-date-wrap field"><h4 class="event-date-first">' . $datemonthday . '</h4>';
    $newdate .= '<h5 class="event-date-second">';
    $newdate .=   '<span>'.$dateplainday.'</span>';
    $newdate .=   '<span>'.$datetime.'</span>';
    $newdate .= '</h5></div>';

    //all the fun preprocessed fields for DS
    $vars['event_custom_time'] = $newdate;
    $vars['event_custom_venue'] = '';
    $vars['event_custom_map_1_col'] = '';
    $vars['event_custom_map_2_col'] = '';
    $vars['event_custom_website'] = '';
    $vars['event_custom_share'] = '';
    $vars['hero_promo_image'] = '';
    $vars['hero_venue'] = '';
    $vars['hero_title'] = '<span class="h-who">'.l(views_trim_text(array('max_length'=>24, 'ellipsis' => TRUE), $node->title), 'node/'.$node->nid) .'</span>';
    $vars['upcoming_when'] = '';
    $vars['upcoming_what_where'] = '';
    $vars['upcoming_how'] = '';
    $vars['upcoming_repeat'] = '';

    $maprendered = '';

    //customize the upcoming_when variable
    $upcomingwhen = '<p class="item_when">';
    $upcomingwhen .= format_date($datestring, 'short_month_and_day');
    $upcomingwhen .= ' <span class="light_content_text">'.format_date($datestring, 'short_day_characters') .'</span>';
    $upcomingwhen .= '<time>'.format_date($datestring, 'time').'</time>';
    $upcomingwhen .= '</p>';
    $vars['upcoming_when'] = $upcomingwhen;

    //customize the upcoming_what_where variable
    $upcomingwhere = '<span class="item-middle-wrap">';
    $upcomingwhere .= '<p class="item_what">' . l(t($node->title), 'node/'.$node->nid) . '</p>';
    $upcomingwhere .= '<p class="item_where">';

    //customize the upcoming_how variable. Provide a link to purchase tickets if available
    $upcominghow = '<p class="item_how"><span class="get_something">';

    if(count($node->field_ticket_link) > 0):
      $upcominghow .= l(t('Get Tickets'), $node->field_ticket_link['und'][0]['url'], array('attributes' => array('class' => array('get_tickets', 'ticket-me'), 'target' => '_blank')));
    else:
      $upcominghow .= l(t('More Info'), 'node/'.$node->nid, array('attributes' => array('class' => array('get_tickets'))));
    endif;

    $upcominghow .= '</span></p>';
    $vars['upcoming_how'] = $upcominghow;

     if (isset($node->field_date['und'][0]['rrule'])) {
      $vars['upcoming_repeat'] .= theme( 'eventstwo_repeat_template', array( 'repeat' => $repeat));
    }

    //check if there's a venue associated with the event
    if(module_exists('morris_events') && count($node->field_event_venue) > 0):

      $customvenue = '';
      $customvenue .= '<div class="field event-where">';
      $venuenid = $node->field_event_venue['und'][0]['target_id'];

      //freakin got to load the venue node because we need 3 fields from it
      $venuenode = node_load($venuenid);

      //change this to load an organization title by nid
      //$venuetitle = morris_events_load_title($venuenid);
      $venuetitle = $venuenode->title;
      $customvenue .= l(t($venuetitle), 'node/'.$venuenid, array('attributes' => array('class' => array('event-where-venue'))));

      $customvenue .= ($venuenode->field_event_address['und'][0]['thoroughfare'] != '') ? '<p class="event-where-address">' . $venuenode->field_event_address['und'][0]['thoroughfare'] . ' ' . $venuenode->field_event_address['und'][0]['locality'] . '</p>' : '';
      $customvenue .= '</div>';

      $vars['event_custom_venue'] = $customvenue;

      $trimedvenue = views_trim_text(array('max_length'=>36, 'ellipsis' => TRUE), $venuetitle);
      $vars['hero_venue'] = '<span class="h-where"> @ ' . l(t($trimedvenue), 'node/'.$venuenid) . '</span>';

      $upcomingwhere .= l(t($venuetitle),'node/'.$venuenid);

      //if there's not a photo display the map where the photo would have been
      if(count($venuenode->field_event_address) > 0 && $venuenode->field_event_address['und'][0]['thoroughfare'] != ''):
        $themethis = field_view_field('node', $venuenode, 'field_venue_geo', array('label'=>'hidden', 'type'=>'geofield_map_map'));
        $maprendered = render($themethis);
      endif;

    endif;

    //continue customize the upcoming_what_where variable
    $upcomingwhere .= '</p></span>';
    $vars['upcoming_what_where'] = $upcomingwhere;

    if(count($node->field_photo) == 0):
      $vars['event_custom_map_1_col'] = $maprendered;
    else:
      $vars['event_custom_map_2_col'] = $maprendered;
      $herobgimage = 'background-image: url("'.image_style_url('event-feature-hero', $node->field_photo['und'][0]['uri']).'")';
      $vars['hero_promo_image'] = l(t($node->title), 'node/'.$node->nid, array('attributes' => array('class' => array('image-block'), 'style' => $herobgimage)));
    endif;

    //trim the output of the link to stop people from going crazy
    if(count($node->field_event_website) > 0):
      $shortwebsite = views_trim_text(array('max_length'=>20, 'ellipsis' => TRUE), $node->field_event_website['und'][0]['display_url']);
      $vars['event_custom_website'] = l(t($shortwebsite), $node->field_event_website['und'][0]['url'], array('attributes' => array('class' => array('event-website-link'), 'title' => $node->field_event_website['und'][0]['display_url']), 'absolute' => TRUE));
    endif;

    //build share links (Facebook, Twitter, Google+, Mail) for the events detail page
    $nodeurl = url('node/'.$node->nid ,array('absolute' => TRUE));

    $shareparams = array(
                    'u' => $nodeurl
                  );
    $sharefacebook = l(t('Share on Facebook'), 'https://www.facebook.com/sharer/sharer.php', array('query' => $shareparams, 'absolute' => TRUE, 'attributes' => array('class' => array('share--button', 'share--button__facebook'), 'target' => '_blank')));

    $shareparams = array(
                    'source' => $nodeurl,
                    'text' => $node->title . ': ' . $nodeurl
                  );
    $sharetwitter = l(t('Share on Twitter'), 'https://twitter.com/intent/tweet', array('query' => $shareparams, 'absolute' => TRUE, 'attributes' => array('class' => array('share--button', 'share--button__twitter'), 'target' => '_blank')));

    $shareparams = array(
                    'url' => $nodeurl
                  );
    $sharegoogle = l(t('Share on Google+'), 'https://plus.google.com/share', array('query' => $shareparams, 'absolute' => TRUE, 'attributes' => array('class' => array('share--button', 'share--button__google'), 'target' => '_blank')));

    $shareparams = array(
                    'subject' => $node->title,
                    'body' => $nodeurl
                  );
    $sharemail = l(t('Mail'), 'mailto:', array('query' => $shareparams, 'absolute' => TRUE, 'attributes' => array('class' => array('share--button', 'share--button__mail'), 'target' => '_blank')));

    $vars['event_custom_share'] = '<div class="share-wrap">' . $sharefacebook . $sharetwitter . $sharegoogle . $sharemail . '</div>';

  } //end if node->type == event

}

/*
function eventstwo_preprocess_comment(&$vars,$hook) {
  //  kpr($vars['content']);
}
*/

/*
 * Horrible idea. We need to build HTML for an external widget using php.
 * This is then served up through views as json
 */

function eventstwo_views_pre_render(&$view) {
  if($view->name == 'mpg_events_feed' && $view->current_display == 'page_1'):

    $i = 0;
    $l = 7;
    $days = array();

    for (; $i < $l; $i++):

      $timestring = '+' . $i . ' day';
      $day = strtotime($timestring);

      $date = format_date($day ,'custom','j');
      $daynumber = format_date($day ,'custom','w');
      $month = format_date($day ,'custom','n');
      $year = format_date($day ,'custom','Y');

      if($i == 0):
        $today = $daynumber;
      endif;

      $parameters = array(
                'field_date_value[value][year]' => $year,
                'field_date_value[value][month]' => $month,
                'field_date_value[value][day]' => $date
              );

      $todaysLetter = '';
      switch($daynumber) {
        case 1:
          $todaysLetter = 'm';
          break;
        case 2:
        case 4:
          $todaysLetter = 't';
          break;
        case 3:
          $todaysLetter = 'w';
          break;
        case 5:
          $todaysLetter = 'f';
          break;
        default:
          $todaysLetter = 's';
      }

      $days[$daynumber] = l(t($todaysLetter), 'events', array('query' => $parameters, 'absolute' => TRUE));
      //http://mpgdev.dev3.webenabled.net/events?field_date_value[value][year]=2013&field_date_value[value][month]=10&field_date_value[value][day]=20

    endfor;

    //put the days in the proper order
    ksort($days);

    $finallist = '';
    foreach($days as $key => $day):
      $todayclass = ($key == $today)? ' class="events-today"': '';
      $finallist .= '<li' . $todayclass . '>' . $day . '</li>'; //events-today
    endforeach;

    $view->result[0]->field_field_date_2[0]['rendered']['#markup'] = $finallist;

  endif;
}

/*
function eventstwo_preprocess_maintenance_page(){
  //  kpr($vars['content']);
}
*/

function eventstwo_menu_tree__main_menu($variables) {
 return '<ul id="top_navi_list" class="navi_list">' . $variables['tree'] . '</ul>';
}

//theme the main menu
function eventstwo_menu_link__main_menu(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  $element['#attributes']['class'][] = 'large-list-item';
  $element['#localized_options']['attributes']['class'] = array('nav_link');

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}



/**
 * Implements hook_theme().
 */
function eventstwo_theme($existing, $type, $theme, $path) {
  if ($type === 'theme') {
    return array(
      'eventstwo_repeat_template' => array(
        'template'=>'repeat',
        'path'=>$path.'/templates',
        'type' => 'theme',
        'variables' => array('repeat'=>NULL)
      ),
    );
  }
  return array();
}