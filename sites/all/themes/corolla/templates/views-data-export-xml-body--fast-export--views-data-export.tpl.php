<?php
/**
 * @file views-view-table.tpl.php
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $rows: An array of row items. Each row is an array of content
 *   keyed by field ID.
 * - $header: an array of headers(labels) for fields.
 * - $themed_rows: a array of rows with themed fields.
 * @ingroup views_templates
 */
?>
<?php foreach ($themed_rows as $count => $row): ?>
<?php //print_r($row);?>
<?php 
if ($row['field_date']){
	$date1=explode('T',$row['field_date']);
	$start_date=$date1[0];
	$start_time=$date1[1];
}
if ($row['field_date_1']){
	$date2=explode('T',$row['field_date_1']);
	$end_date=$date2[0];
	$end_time=$date2[1];
}
?>
  <document uniqueid="<?php print variable_get('fast_editorial_business_unit_identifier', '');?>-CAL-<?php print $row['nid'];?>">
  <element name="businessunit"><value><![CDATA[<?php print variable_get('fast_editorial_business_unit_identifier', '');?>]]></value></element>
<element name="businessuniturl"><value><![CDATA[<?php print variable_get('fast_editorial_business_unit_url', '');?>]]></value></element>
<element name="contentclass"><value><![CDATA[Events]]></value></element>
<element name="contentsource"><value><![CDATA[Drupal]]></value></element>
<element name="contentid"><value><![CDATA[<?php print $row['nid'];?>]]></value></element>
<element name="contentstatus"><value><![CDATA[active]]></value></element>
<element name="creationtime"><value><![CDATA[<?php print $row['created'];?>]]></value></element>
<element name="modificationtime"><value><![CDATA[<?php print $row['changed'];?>]]></value></element>
<element name="body"><value><![CDATA[<?php print $row['body'];?>]]></value></element>
<element name="contenttype"><value><![CDATA[text/html]]></value></element>
<element name="title"><value><![CDATA[<?php print $row['title'];?>]]></value></element>
<element name="url"><value><![CDATA[<?php print $row['path'];?>]]></value></element>
<element name="private"><value><![CDATA[public]]></value></element>
<element name="teaser"><value><![CDATA[<?php print $row['body'];?>]]></value></element>
<element name="imageurl"><value><![CDATA[<?php print $row['field_photo'];?>]]></value></element>
<element name="categories"><value><![CDATA[]]></value></element>
<element name="generic1"><value><![CDATA[<?php print $row['field_cost'];?>]]></value></element>

<element name="eventstartdate"><value><![CDATA[<?php print $start_date;?>]]></value></element>
<element name="eventstarttime"><value><![CDATA[<?php print $start_time;?>]]></value></element>
<element name="eventstopdate"><value><![CDATA[<?php print $end_date;?>]]></value></element>
<element name="eventstoptime"><value><![CDATA[<?php print $end_time;?>]]></value></element>
<element name="eventurl"><value><![CDATA[<?php print $row['field_website'];?>]]></value></element>
<element name="venue"><value><![CDATA[<?php print $row['field_venue'];?>]]></value></element>
<element name="organization"><value><![CDATA[<?php print $row['field_venue'];?>]]></value></element>
<element name="locationname"><value><![CDATA[<?php print $row['name'];?>]]></value></element>
<element name="city"><value><![CDATA[<?php print $row['city'].','.$row['province'];?>]]></value></element>
<element name="tags"><value><![CDATA[<?php print $row['field_tags'];?>]]></value></element>

  </document>
<?php endforeach; ?>
