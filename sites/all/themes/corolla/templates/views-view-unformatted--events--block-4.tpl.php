<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<?php
	//we only want ONE of the rows that are returned So let's count it
	$counter = 0;
	GLOBAL $overallcount;
	
	foreach ($rows as $id => $row):
		
		$counter++;
		
		if($counter == 1 && $overallcount <= 4):
			
			//we keep track of how many are actually being displayed
			//this allows us to ONLY display 5.
			//this is a work around for the stupid group by views stuff
			
			$overallcount++;
?>
		  <div class="<?php print $classes_array[$id]; ?>">
		  
			<?php print $row; ?>
			
		  </div>
		
  <?php endif; ?>

<?php endforeach; ?>