<div id="widget">

        <div id="w-content">
                <?php print render($page['content']);?>
        </div>

        <div id="w-dates">
                        <div class="clearfix">

                                <div class="view-content">

                                <?php

                                        $today = date('Y-m-d');


                                        $dbresult = db_query("SELECT DATE(d.field_date_value) w from {node} n, {field_data_field_date} d WHERE n.type = 'event' AND d.entity_id = n.nid AND DATE(d.field_date_value) >= :today GROUP BY w ORDER BY w ASC limit 5;",array(':today' => $today));

                                        foreach($dbresult as $row){

                                          $unixdate = strtotime($row->w);
                                          $D = date("D",$unixdate);
                                          $d = date("d",$unixdate);

                                          echo '<div class="views-row">
                                                                <a title="View events for '.$row->w.'" href="/calendar/day/'.$row->w.'"  target="_blank">
                                                                <h5>'.$D.'</h5>
                                                                <h6>'.$d.'</h6>
                                                                </a>
                                                        </div>';
                                        }

                                ?>

                                </div>

                  <div class="view-footer clearfix">
                                <p><a href="/events-all" target="_blank">View All Events</a></p>
                                <p><a href="/node/add/event" target="_blank">+ Add a Listing</a></p>
                  </div>


                </div>
        </div>

</div><!--/widget-->
